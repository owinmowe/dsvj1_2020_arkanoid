#include "draw.h"
#include "raylib.h"
#include "Arkanoid/Scenes/scene_manager.h"
#include "Arkanoid/ExternalVariables/external_variables.h"

namespace screenRelated
{

    int currentFrame = 0;

    //Modifies size/pos to adjust to screen size being 0 null and 1 fullscreen 
    Vector2 screenModifier(Vector2 size)
    {
        Vector2 newVector2;
        newVector2.x = size.x * SCREEN_WIDTH;
        newVector2.y = size.y * SCREEN_HEIGHT;
        return newVector2;
    }

}

void frameCounter();

void drawGame()
{

    frameCounter();
    BeginDrawing();
    ClearBackground(BLACK);
    sceneRelated::currentScene_Ptr->draw();
    EndDrawing();

}

void frameCounter()
{

    using namespace screenRelated;

    currentFrame++;
    if(currentFrame == FRAMES_PER_SECOND)
    {
        currentFrame = 0;
    }

}

