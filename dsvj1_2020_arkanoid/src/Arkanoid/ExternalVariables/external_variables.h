#ifndef EXTERNAL_VARIABLES_H
#define EXTERNAL_VARIABLES_H

#include "raylib.h"
#include "Arkanoid/Input/input.h"

namespace screenRelated
{

	const int SCREEN_WIDTH = 1920;
	const int SCREEN_HEIGHT = 1080;
	const int FRAMES_PER_SECOND = 60;
	extern int currentFrame;
	Vector2 screenModifier(Vector2 size);

}

namespace inputRelated
{

	extern newKeycodes currentKeycodes;
	extern Input currentInput;

}

namespace in_game 
{
	namespace gameScoreRelated
	{
		extern int currentScore;
		extern int currentLifes;
		extern int remainingBlocks;
	}
}



#endif EXTERNAL_VARIABLES_H