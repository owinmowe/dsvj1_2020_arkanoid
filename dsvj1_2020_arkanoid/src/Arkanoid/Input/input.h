#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace inputRelated
{

	struct newKeycodes
	{

		int enter = KEY_ENTER;
		int pause = KEY_P;
		int special = KEY_SPACE;

		int down_Left = KEY_A;
		int down_Down = KEY_S;
		int down_Up = KEY_W;
		int down_Right = KEY_D;

		int pressed_Left = KEY_A;
		int pressed_Down = KEY_S;
		int pressed_Up = KEY_W;
		int pressed_Right = KEY_D;

	};

	const struct defaultKeycodes
	{

		int enter = KEY_ENTER;
		int pause = KEY_P;
		int special = KEY_SPACE;

		int down_Left = KEY_A;
		int down_Down = KEY_S;
		int down_Up = KEY_W;
		int down_Right = KEY_D;

		int pressed_Left = KEY_A;
		int pressed_Down = KEY_S;
		int pressed_Up = KEY_W;
		int pressed_Right = KEY_D;

	};

	const defaultKeycodes DEFAULT_KEYCODES;

	struct Input
	{

		bool enter = false;
		bool pause = false;
		bool special = false;

		bool down_Left = false;
		bool down_Down = false;
		bool down_Up = false;
		bool down_Right = false;

		bool pressed_Left = false;
		bool pressed_Down = false;
		bool pressed_Up = false;
		bool pressed_Right = false;

	};

	void inputGame();
	Input checkInput();
	void resetKeycodes();
	void changeCurrentKeycode(int& keycode);
	bool validateKeycode(int newKeycode, int currentKeycode);

}
#endif INPUT_H
