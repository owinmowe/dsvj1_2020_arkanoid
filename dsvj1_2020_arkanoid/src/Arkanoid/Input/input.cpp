#include "input.h"
#include "Arkanoid/ExternalVariables/external_variables.h"
#include "Arkanoid/Scenes/change_keys.h"
#include "Arkanoid/Scenes/scene_manager.h"

namespace inputRelated
{

    newKeycodes currentKeycodes;

    Input currentInput;

    void inputGame()
    {

        inputRelated::currentInput = checkInput();

    }

    Input checkInput()
    {

        using namespace inputRelated;

        Input newInput;

        if (IsKeyPressed(currentKeycodes.enter)) { newInput.enter = true; }
        if (IsKeyPressed(currentKeycodes.pause)) { newInput.pause = true; }
        if (IsKeyPressed(currentKeycodes.special)) { newInput.special = true; }

        if (IsKeyDown(currentKeycodes.down_Left)) { newInput.down_Left = true; }
        if (IsKeyDown(currentKeycodes.down_Down)) { newInput.down_Down = true; }
        if (IsKeyDown(currentKeycodes.down_Up)) { newInput.down_Up = true; }
        if (IsKeyDown(currentKeycodes.down_Right)) { newInput.down_Right = true; }

        if (IsKeyPressed(currentKeycodes.pressed_Left)) { newInput.pressed_Left = true; }
        if (IsKeyPressed(currentKeycodes.pressed_Down)) { newInput.pressed_Down = true; }
        if (IsKeyPressed(currentKeycodes.pressed_Up)) { newInput.pressed_Up = true; }
        if (IsKeyPressed(currentKeycodes.pressed_Right)) { newInput.pressed_Right = true; }

        return newInput;

    }

    void resetKeycodes()
    {

        inputRelated::currentKeycodes.down_Down = DEFAULT_KEYCODES.down_Down;
        inputRelated::currentKeycodes.down_Left = DEFAULT_KEYCODES.down_Left;
        inputRelated::currentKeycodes.down_Right = DEFAULT_KEYCODES.down_Right;
        inputRelated::currentKeycodes.down_Up = DEFAULT_KEYCODES.down_Up;
        inputRelated::currentKeycodes.enter = DEFAULT_KEYCODES.enter;
        inputRelated::currentKeycodes.pause = DEFAULT_KEYCODES.pause;
        inputRelated::currentKeycodes.pressed_Down = DEFAULT_KEYCODES.pressed_Down;
        inputRelated::currentKeycodes.pressed_Left = DEFAULT_KEYCODES.pressed_Left;
        inputRelated::currentKeycodes.pressed_Right = DEFAULT_KEYCODES.pressed_Right;
        inputRelated::currentKeycodes.pressed_Up = DEFAULT_KEYCODES.pressed_Up;
        inputRelated::currentKeycodes.special = DEFAULT_KEYCODES.special;

    }
    void changeCurrentKeycode(int& keycode)
    {

        int auxKeycode = 0;

        if (IsKeyPressed(KEY_UP))
        {
            auxKeycode = KEY_UP;
        }
        else if (IsKeyPressed(KEY_DOWN))
        {
            auxKeycode = KEY_DOWN;
        }
        else if (IsKeyPressed(KEY_LEFT))
        {
            auxKeycode = KEY_LEFT;
        }
        else if (IsKeyPressed(KEY_RIGHT))
        {
            auxKeycode = KEY_RIGHT;
        }
        else if (IsKeyPressed(KEY_ENTER))
        {
            auxKeycode = KEY_ENTER;
        }
        else if (IsKeyPressed(KEY_SPACE))
        {
            auxKeycode = KEY_SPACE;
        }
        else
        {
            auxKeycode = GetKeyPressed();
            if (auxKeycode != 0)
            {
                auxKeycode -= 32; //GetKeyPressed devuelve solo caracteres, por lo tanto devuelve letras minusculas a diferencia de los Keycodes
            }
        }

        if(validateKeycode(auxKeycode, keycode))
        {
            keycode = auxKeycode;
            currentKeycodes.down_Down = currentKeycodes.pressed_Down;
            currentKeycodes.down_Up = currentKeycodes.pressed_Up;
            currentKeycodes.down_Left = currentKeycodes.pressed_Left;
            currentKeycodes.down_Right = currentKeycodes.pressed_Right;
            ui::events::selectEvent = true;
            static_cast<change_keys::ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey();
        }

    }

    bool validateKeycode(int newKeycode, int currentKeycode)
    {
        if(newKeycode == currentKeycode)
        {
            return true;
        }
        else if(newKeycode != 0 &&
                newKeycode != inputRelated::currentKeycodes.down_Down &&
                newKeycode != inputRelated::currentKeycodes.down_Left &&
                newKeycode != inputRelated::currentKeycodes.down_Right &&
                newKeycode != inputRelated::currentKeycodes.down_Up &&
                newKeycode != inputRelated::currentKeycodes.enter &&
                newKeycode != inputRelated::currentKeycodes.pause &&
                newKeycode != inputRelated::currentKeycodes.special)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}