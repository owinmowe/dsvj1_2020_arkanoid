#ifndef UI_H
#define UI_H

#include "raylib.h"
#include <string>

namespace ui
{

	const float BUTTON_BASE_WIDTH = 1 / 4.f;
	const float BUTTON_BASE_HEIGHT = 1 / 10.f;
	const float SLIDER_BASE_WIDTH = 1 / 4.f;
	const float SLIDER_BASE_HEIGHT = 1 / 40.f;

	const Vector2 SLIDER_HANDLE_SIZE = { 0.030f ,  0.038f };
	const int SLIDER_MAX_HANDLE_POSITIONS = 10;
	extern Vector2 buttonSize;
	extern Vector2 SliderSize;

	const int maxSizeTextButton = 25;

	class GUIcomponent
	{
	public:
		bool selected = false;
		virtual void update();
		virtual void drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i);
		virtual void drawComponent(float posX, float posY);

	private:
	};
	void drawUpDownGUI(GUIcomponent* GUI_Ptr[], int GUI_SIZE, const float GUI_STARTING_POSITION, const float GUI_SEPARATION);
	void upDownGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void setGUITexturesSize();
	const float TEXT_SIZE_SCALE = 0.0208f;
	const Color GUI_SELECTED_COLOR = BLUE;
	const Color GUI_NOT_SELECTED_COLOR = WHITE;

	class Button : public GUIcomponent
	{
	public:
		void drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i) override;
		void drawComponent(float posX, float posY) override;
		virtual void action();
		virtual std::string getText();
	private:
		void update() override;
		std::string text;
	};
	const char BUTTON_PATH_FILE[] = "res/assets/texture/menu/button.png";

	class Slider : public GUIcomponent
	{
	public:
		void drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i) override;
		virtual void leftAction();
		virtual void rightAction();
		virtual std::string getText();
		int handlePosition = 0;
	private:
		void update() override;
		std::string text;
	};
	const char SLIDER_BAR_PATH_FILE[] = "res/assets/texture/menu/sliderBar.png";
	const char SLIDER_HANDLE_PATH_FILE[] = "res/assets/texture/menu/sliderHandle.png";

	void backgroundAnimation();
	const int BG_MAX_FADE = 125;
	const int flickerSpeed = 1;
	const char BACKGROUND_PATH_FILE[] = "res/assets/texture/background.png";

	const char RAYLIBLOGO_PATH_FILE[] = "res/assets/texture/menu/Raylib_logo.png";

	void drawTextWithSecondFont(const char* text, float posX, float posY, int fontSize, Color color);
	const char FONT_PATH_FILE[] = "res/assets/fonts/digitalo.ttf";

	float textScreenModifier(float size);

	const char LEVEL_CHANGE_PATH_FILE[] = "res/assets/texture/levelChange.png";

	namespace textures
	{

		extern Texture2D backPauseTexture;
		extern Texture2D overlayTexture;
		extern Texture2D backgroundTexture;
		extern Texture2D levelChangeTexture;

		extern SpriteFont myfont;

		namespace menu
		{

			extern Texture2D buttonTexture;
			extern Texture2D sliderBarTexture;
			extern Texture2D sliderHandleTexture;
			extern Texture2D raylibLogoTexture;

		}

	}

	namespace events
	{

		extern bool moveEvent;
		extern bool selectEvent;

		void buttonEventReset();

	}

}

#endif UI_H