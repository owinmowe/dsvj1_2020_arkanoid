#include "ui.h"
#include "Arkanoid/Scenes/scene_manager.h"
#include "Arkanoid/ExternalVariables/external_variables.h"

namespace ui
{

	Vector2 buttonSize;
	Vector2 SliderSize;

	namespace textures
	{

		Texture2D backPauseTexture;
		Texture2D overlayTexture;
		Texture2D backgroundTexture;
		Texture2D levelChangeTexture;

		SpriteFont myfont;

		namespace menu
		{

			Texture2D buttonTexture;
			Texture2D sliderBarTexture;
			Texture2D sliderHandleTexture;
			Texture2D raylibLogoTexture;

		}

	}

	namespace events
	{

		bool moveEvent;
		bool selectEvent;

		void buttonEventReset()
		{

			if (events::moveEvent) { events::moveEvent = false; }
			if (events::selectEvent) { events::selectEvent = false; }

		}

	}

	void GUIcomponent::update()
	{
		return;
	}
	void GUIcomponent::drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i)
	{
		return;
	}
	void GUIcomponent::drawComponent(float posX, float posY)
	{
		return;
	}

	std::string Button::getText()
	{
		return text;
	}
	void Button::drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i)
	{

		Color buttonColor;
		selected ? buttonColor = GUI_SELECTED_COLOR : buttonColor = GUI_NOT_SELECTED_COLOR;
		Vector2 newPos = { screenRelated::SCREEN_WIDTH / 2 - screenRelated::screenModifier(buttonSize).x / 2, screenRelated::SCREEN_HEIGHT * GUI_STARTING_POSITION + screenRelated::SCREEN_HEIGHT * i * GUI_SEPARATION };
		DrawTexture(textures::menu::buttonTexture, static_cast<int>(newPos.x), static_cast<int>(newPos.y), buttonColor);

		char auxText[20];
		strcpy_s(auxText, getText().c_str());
		newPos.x += screenRelated::screenModifier(buttonSize).x / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		newPos.y += screenRelated::screenModifier(buttonSize).y / 2 - textScreenModifier(TEXT_SIZE_SCALE) / 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);

	}
	void Button::drawComponent(float posX, float posY)
	{
		Color buttonColor;
		selected ? buttonColor = GUI_SELECTED_COLOR : buttonColor = GUI_NOT_SELECTED_COLOR;
		Vector2 newPos = { posX * screenRelated::SCREEN_WIDTH - screenRelated::screenModifier(buttonSize).x / 2, screenRelated::SCREEN_HEIGHT * posY - screenRelated::screenModifier(buttonSize).y / 2 };
		DrawTexture(textures::menu::buttonTexture, static_cast<int>(newPos.x), static_cast<int>(newPos.y), buttonColor);

		char auxText[20];
		strcpy_s(auxText, getText().c_str());
		newPos.x += screenRelated::screenModifier(buttonSize).x / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		newPos.y += screenRelated::screenModifier(buttonSize).y / 2 - textScreenModifier(TEXT_SIZE_SCALE) / 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);
	}
	void Button::action()
	{
		return;
	}
	void Button::update()
	{
		if(inputRelated::currentInput.enter)
		{
			events::selectEvent = true;
			action();
		}
	}

	void Slider::leftAction()
	{
		return;
	}
	void Slider::rightAction()
	{
		return;
	}
	void Slider::update()
	{
		if (inputRelated::currentInput.pressed_Right && handlePosition < SLIDER_MAX_HANDLE_POSITIONS)
		{
			events::selectEvent = true;
			handlePosition++;
			rightAction();
		}
		else if (inputRelated::currentInput.pressed_Left && handlePosition > 0)
		{
			events::selectEvent = true;
			handlePosition--;
			leftAction();
		}
	}
	void Slider::drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i)
	{
		Color sliderColor;
		selected ? sliderColor = GUI_SELECTED_COLOR : sliderColor = GUI_NOT_SELECTED_COLOR;
		Vector2 newPos = { (static_cast<float>(screenRelated::SCREEN_WIDTH / 2 - screenRelated::screenModifier(SliderSize).x / 2)), static_cast<float>(screenRelated::SCREEN_HEIGHT * GUI_STARTING_POSITION + screenRelated::SCREEN_HEIGHT * i * GUI_SEPARATION + screenRelated::screenModifier(SliderSize).y / 2) };
		Vector2 handlePos = newPos;
		DrawTexture(textures::menu::sliderBarTexture, static_cast<int>(newPos.x), static_cast<int>(newPos.y), WHITE);

		char auxText[20];
		strcpy_s(auxText, getText().c_str());
		newPos.x += screenRelated::screenModifier(SliderSize).x / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		newPos.y += screenRelated::screenModifier(SliderSize).y / 2 - textScreenModifier(TEXT_SIZE_SCALE) * 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);

		Vector2 offset{0, 0.005f};
		handlePos.x += (handlePosition * screenRelated::screenModifier(SliderSize).x / 10) - textures::menu::sliderHandleTexture.width / 2;
		handlePos.y -= screenRelated::screenModifier(offset).y;
		DrawTexture(textures::menu::sliderHandleTexture, static_cast<int>(handlePos.x), static_cast<int>(handlePos.y), sliderColor);

	}
	std::string Slider::getText()
	{
		return text;
	}


	void upDownGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{

		events::buttonEventReset();

		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUI_Ptr[i]->selected)
			{
				GUI_Ptr[i]->update();
				break;
			}
		}

		if (inputRelated::currentInput.pressed_Down)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i + 1 < GUI_SIZE)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i + 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
		else if (inputRelated::currentInput.pressed_Up)
		{
			for (int i = GUI_SIZE - 1; i >= 0; i--)
			{
				if (GUI_Ptr[i]->selected && i > 0)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i - 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && GUI_SIZE - 1 != 0)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[GUI_SIZE - 1]->selected = true;
					break;
				}
			}
		}
	}

	void backgroundAnimation()
	{

		static bool up = true;

		static Color bgColor = { BG_MAX_FADE, BG_MAX_FADE, BG_MAX_FADE, 255 };

		if (up)
		{
			bgColor.r += flickerSpeed;
			bgColor.g += flickerSpeed;
			bgColor.b += flickerSpeed;
		}
		else
		{
			bgColor.r -= flickerSpeed;
			bgColor.g -= flickerSpeed;
			bgColor.b -= flickerSpeed;
		}

		DrawTexture(textures::backgroundTexture, 0, 0, bgColor);

		if (bgColor.b >= 255 - flickerSpeed)
		{
			up = false;
		}
		else if (bgColor.b <= BG_MAX_FADE + flickerSpeed)
		{
			up = true;
		}

	}
	void drawUpDownGUI(GUIcomponent* GUI_Ptr[], const int GUI_SIZE, const float GUI_STARTING_POSITION, const float GUI_SEPARATION)
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			GUI_Ptr[i]->drawComponent(GUI_STARTING_POSITION, GUI_SEPARATION, i);
		}

	}

	void setGUITexturesSize()
	{

		textures::menu::buttonTexture.width = static_cast<int>(screenRelated::screenModifier(buttonSize).x);
		textures::menu::buttonTexture.height = static_cast<int>(screenRelated::screenModifier(buttonSize).y);
		textures::menu::sliderBarTexture.width = static_cast<int>(screenRelated::screenModifier(SliderSize).x);
		textures::menu::sliderBarTexture.height = static_cast<int>(screenRelated::screenModifier(SliderSize).y);
		textures::menu::sliderHandleTexture.width = static_cast<int>(screenRelated::screenModifier(SLIDER_HANDLE_SIZE).x);
		textures::menu::sliderHandleTexture.height = static_cast<int>(screenRelated::screenModifier(SLIDER_HANDLE_SIZE).y);

	}

	void drawTextWithSecondFont(const char* text, float posX, float posY, int fontSize, Color color)
	{
		DrawTextEx(textures::myfont, text, { posX, posY }, static_cast<float>(fontSize), 1, color);
	}

	float textScreenModifier(float size)
	{
		return static_cast<float>(size * screenRelated::SCREEN_WIDTH);
	}
}