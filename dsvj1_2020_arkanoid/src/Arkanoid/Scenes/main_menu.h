#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "scenes_base.h"
#include "raylib.h"
#include "Arkanoid/UI/ui.h"

namespace main_menu
{

	using namespace ui;

	const char TITLE_TEXT[] = "SPACE ARKANOID";
	const float TITLE_SIZE = 1 / 10.f;
	const float TITLE_POSITION = 1 / 16.f;

	const int GUI_SIZE = 4;
	const float GUI_WIDTH = 3 / 18.f;
	const float GUI_HEIGHT = 3 / 20.f;
	const float GUI_STARTING_POSITION = 1 / 3.f;
	const float GUI_SEPARATION = 1 / 6.f;

	class MainMenu : public SceneBase
	{

	private:
		void init() override;
		void deInit() override;
		GUIcomponent* GUI_Ptr[GUI_SIZE];

	public:
		MainMenu();
		~MainMenu();
		void update() override;
		void directKeysLogic();
		void draw() override;

	};

	class PlayButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Play";
	};

	class OptionsButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Options";
	};

	class CreditsButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Credits";
	};

	class ExitButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Exit";
	};

}

#endif MAIN_MENU_H
