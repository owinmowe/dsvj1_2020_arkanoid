#include "in_game.h"
#include "raylib.h"
#include "Arkanoid/ExternalVariables/external_variables.h"
#include "scene_manager.h"
#include "Arkanoid/Audio/audio.h"
#include "Arkanoid/Collision/collision.h"
#include "level_loader.h"

namespace in_game
{

	namespace gameScoreRelated
	{
		int currentScore = 0;
		int currentLifes = LIFES_PER_GAME;
		int remainingBlocks = 0;
	}

	InGame::InGame()
	{
		init();
	}
	InGame::~InGame()
	{
		deInit();
	}
	void InGame::init()
	{

		StopMusicStream(audio::musicVariables.MenuMusic);
		StopMusicStream(audio::musicVariables.GameMusic);
		PlayMusicStream(audio::musicVariables.GameMusic);
		loadTextures();
		setGameObjects();
		setGUI();
		gameScoreRelated::currentLifes = LIFES_PER_GAME;
		gameScoreRelated::currentScore = 0;
		transitionToNextLevel = true;
		LostState = false;
		restartLevel();

	}
	void InGame::deInit()
	{

		unloadTextures();
		deleteGameObjects();

	}
	void InGame::update()
	{

		if (sceneRelated::pause)
		{
			pauseLogic();
		}
		else if(LostState)
		{
			lostScreenLogic();
		}
		else
		{

			gameObjects::events::resetGameObjectsEvents();		
			directKeysLogic();
			ballsLogic();
			collisionDetection(pad_Ptr, power_ups_Ptr, gameObjects::POWER_UPS_AMMOUNT, balls_Ptr, bricks_Ptr, gameObjects::BRICKS_COL, gameObjects::BRICKS_ROW, bound_Ptr);
			powerUpsLogic();
			audio::gameAudio();
			levelLogic();
		}

	}

	void InGame::lostScreenLogic()
	{
		if (inputRelated::currentInput.enter)
		{
			sceneRelated::ChangeSceneTo(sceneRelated::Scene::MAIN_MENU);
		}
	}

	void InGame::draw()
	{

		if (LostState)
		{
			ClearBackground(BLACK);
			DrawText(FormatText("YOU LOST. YOUR FINAL SCORE WAS: %08i \nPRESS ENTER TO GO BACK TO THE MENU", gameScoreRelated::currentScore), screenRelated::SCREEN_WIDTH / 2 -
				MeasureText(FormatText("YOU LOST. YOUR FINAL SCORE WAS: %08i \nPRESS ENTER TO GO BACK TO THE MENU", gameScoreRelated::currentScore), ui::textScreenModifier(NEXT_LEVEL_SIZE)) / 2,
				screenRelated::SCREEN_HEIGHT / 2 - ui::textScreenModifier(NEXT_LEVEL_SIZE) / 2, ui::textScreenModifier(NEXT_LEVEL_SIZE), WHITE);
		}
		else
		{
			ui::backgroundAnimation();
			drawGameObjects();
			nextlevelAnimation();
			DrawTexture(ui::textures::overlayTexture, 0, 0, WHITE);
			DrawTexture(gameObjects::textures::outerBoundsTexture, static_cast<int>(bound_Ptr->rec.x), static_cast<int>(bound_Ptr->rec.y), WHITE);
			DrawText(FormatText("Score: %08i", gameScoreRelated::currentScore), screenRelated::SCREEN_WIDTH * 1 / 32, screenRelated::SCREEN_HEIGHT * 1 / 4, 20, WHITE);
			DrawText(FormatText("Remaining Lifes: %i", gameScoreRelated::currentLifes), screenRelated::SCREEN_WIDTH * 1 / 32, screenRelated::SCREEN_HEIGHT * 3 / 8, 20, WHITE);
		}

#if DEBUG
		drawDebug();
#endif // DEBUG
		if (sceneRelated::pause)
		{
			drawPauseUI();
		}
	}
	void InGame::setGUI()
	{
		GUI_Ptr[0] = new Unpause;
		GUI_Ptr[1] = new BackToMenuButton;
		GUI_Ptr[0]->selected = true;
		sceneRelated::pause = false;
		gameScoreRelated::currentScore = 0;
	}
	void InGame::setGameObjects()
	{
		bound_Ptr = new gameObjects::Bound;

		pad_Ptr = new gameObjects::Pad;
		pad_Ptr->rec.x = screenRelated::SCREEN_WIDTH / 2 - pad_Ptr->rec.width / 2;
		pad_Ptr->rec.y = screenRelated::SCREEN_HEIGHT * 7 / 8;

		for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
		{
			balls_Ptr[i] = new gameObjects::Ball;
		}

//#if DEBUG

		levelLoader::loadLevel(1, bricks_Ptr, BRICKS_COL, BRICKS_ROW, gameScoreRelated::remainingBlocks);
		gameScoreRelated::remainingBlocks = 0;

//#endif

		for (int i = 0; i < POWER_UPS_AMMOUNT; i++)
		{
			power_ups_Ptr[i] = new PowerUp;
		}

	}
	void InGame::loadTextures()
	{

		ui::textures::levelChangeTexture = LoadTexture(ui::LEVEL_CHANGE_PATH_FILE);
		ui::textures::levelChangeTexture.width = screenRelated::SCREEN_WIDTH;
		ui::textures::levelChangeTexture.height = screenRelated::SCREEN_HEIGHT;

		ui::textures::overlayTexture = LoadTexture(OVERLAY_PATH_FILE);
		ui::textures::overlayTexture.width = screenRelated::SCREEN_WIDTH;
		ui::textures::overlayTexture.height = screenRelated::SCREEN_HEIGHT;

		ui::textures::backPauseTexture = LoadTexture(PAUSE_OVERLAY_PATH_FILE);
		ui::textures::backPauseTexture.width = static_cast<int>(PAUSE_OVERLAY_REC.width);
		ui::textures::backPauseTexture.height = static_cast<int>(PAUSE_OVERLAY_REC.height);

		gameObjects::textures::outerBoundsTexture = LoadTexture(gameObjects::textures::OUTER_BOUNDS_PATH_FILE);
		gameObjects::textures::outerBoundsTexture.width = gameObjects::BOUND_WIDTH;
		gameObjects::textures::outerBoundsTexture.height = gameObjects::BOUND_HEIGHT;

		gameObjects::textures::padTexture = LoadTexture(gameObjects::textures::PAD_PATH_FILE);
		gameObjects::textures::padTexture.width = static_cast<int>(gameObjects::PAD_BASE_WIDTH * screenRelated::SCREEN_WIDTH);
		gameObjects::textures::padTexture.height = static_cast<int>(gameObjects::PAD_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT);

		gameObjects::textures::ballTexture = LoadTexture(gameObjects::textures::BALL_PATH_FILE);
		gameObjects::textures::ballTexture.width = static_cast<int>(gameObjects::BALL_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);
		gameObjects::textures::ballTexture.height = static_cast<int>(gameObjects::BALL_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);

		gameObjects::textures::fireball = LoadTexture(gameObjects::textures::FIREBALL_PATH_FILE);
		gameObjects::textures::fireball.width = static_cast<int>(gameObjects::BALL_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);
		gameObjects::textures::fireball.height = static_cast<int>(gameObjects::BALL_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);

		gameObjects::textures::commonBrickTexture = LoadTexture(gameObjects::textures::COMMON_BRICK_PATH_FILE);
		gameObjects::textures::commonBrickTexture.width = static_cast<int>(gameObjects::BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH);
		gameObjects::textures::commonBrickTexture.height = static_cast<int>(gameObjects::BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT);

		gameObjects::textures::indestructibleBrickTexture = LoadTexture(gameObjects::textures::INDESTRUCTIBLE_BRICK_PATH_FILE);
		gameObjects::textures::indestructibleBrickTexture.width = static_cast<int>(gameObjects::BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH);
		gameObjects::textures::indestructibleBrickTexture.height = static_cast<int>(gameObjects::BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT);

		gameObjects::textures::explosiveBrickTexture = LoadTexture(gameObjects::textures::EXPLOSIVE_BRICK_PATH_FILE);
		gameObjects::textures::explosiveBrickTexture.width = static_cast<int>(gameObjects::BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH);
		gameObjects::textures::explosiveBrickTexture.height = static_cast<int>(gameObjects::BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT);

		gameObjects::textures::multiballPowerUp = LoadTexture(gameObjects::textures::MULTIBALL_POWER_UP_PATH_FILE);
		gameObjects::textures::multiballPowerUp.width = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);
		gameObjects::textures::multiballPowerUp.height = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);

		gameObjects::textures::fireballPowerUp = LoadTexture(gameObjects::textures::FIREBALL_POWER_UP_PATH_FILE);
		gameObjects::textures::fireballPowerUp.width = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);
		gameObjects::textures::fireballPowerUp.height = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);

		gameObjects::textures::paddlePowerUp = LoadTexture(gameObjects::textures::PADDLE_POWER_UP_PATH_FILE);
		gameObjects::textures::paddlePowerUp.width = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);
		gameObjects::textures::paddlePowerUp.height = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);

		gameObjects::textures::ballSpeedPowerUp = LoadTexture(gameObjects::textures::BALL_SPEED_POWER_UP_PATH_FILE);
		gameObjects::textures::ballSpeedPowerUp.width = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);
		gameObjects::textures::ballSpeedPowerUp.height = static_cast<int>(gameObjects::POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH * 2);

	}
	void InGame::ballsLogic()
	{
		if(!transitionToNextLevel)
		{
			int ballsInGame = 0;
			for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
			{
				if (balls_Ptr[i]->active)
				{

					if (!levelStarted)
					{
						balls_Ptr[0]->moveBallX(static_cast<int>((pad_Ptr->rec.x + pad_Ptr->rec.width / 2)));
					}
					balls_Ptr[i]->updateBall();
					ballsInGame++;
					if (balls_Ptr[i]->pos.y > pad_Ptr->rec.y + pad_Ptr->rec.height * 3 / 2.f)
					{
						balls_Ptr[i]->ballToBase();
						balls_Ptr[i]->active = false;
					}
				}
			}
			if (ballsInGame == 0)
			{
				gameScoreRelated::currentLifes--;
				restartLevel();
			}
		}
	}
	void InGame::powerUpsLogic()
	{

		for (int i = 0; i < gameObjects::BRICKS_COL; i++)
		{
			for (int j = 0; j < gameObjects::BRICKS_ROW; j++)
			{
				if(bricks_Ptr[i][j]->powerUpCreationLogic())
				{
					for (int k = 0; k < POWER_UPS_AMMOUNT; k++)
					{
						if (!power_ups_Ptr[k]->active)
						{
							power_ups_Ptr[k] = getRandomPowerUps();
							power_ups_Ptr[k]->setPosition({ bricks_Ptr[i][j]->rec.x, bricks_Ptr[i][j]->rec.y });
							power_ups_Ptr[k]->active = true;
							bricks_Ptr[i][j]->setDestroyBool(false);
							break;
						}
					}
				}
			}
		}

		for (int i = 0; i < gameObjects::POWER_UPS_AMMOUNT; i++)
		{
			if(power_ups_Ptr[i]->active)
			{
				power_ups_Ptr[i]->updateFall();
			}
		}

		if (events::multiballPowerUp)
		{
			Ball* centerBall_Ptr = balls_Ptr[0];
			for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
			{
				if (balls_Ptr[i]->active)
				{
					centerBall_Ptr = balls_Ptr[i];
					break;
				}
			}
			balls_Ptr[0]->active = true;
			balls_Ptr[0]->pos = centerBall_Ptr->pos;
			balls_Ptr[0]->previousPos = centerBall_Ptr->previousPos;
			balls_Ptr[0]->velocity = centerBall_Ptr->velocity;
			for (int i = 1; i < gameObjects::BALL_MAX_AMMOUNT; i++)
			{
				balls_Ptr[i]->pos = balls_Ptr[0]->pos;
				balls_Ptr[i]->active = true;
				balls_Ptr[i]->pseudoRandomBallDirection(i);
			}
		}
		else if(events::paddlePowerUp)
		{
			pad_Ptr->randomRezise();
		}
		else if (events::fireballPowerUp)
		{
			for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
			{
				if (balls_Ptr[i]->active)
				{
					balls_Ptr[i]->startFireball();
				}
			}
		}
		else if (events::ballSpeedPowerUp)
		{
			for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
			{
				balls_Ptr[i]->startSlowBall();
			}
		}
	}
	void InGame::levelLogic()
	{
		if (gameScoreRelated::remainingBlocks <= 0 && !transitionToNextLevel)
		{
			transitionToNextLevel = true;
			currentLevel++;
			currentTimeToNextLevel = NEXT_LEVEL_MAX_TIME;
		}

		if(currentTimeToNextLevel > 0)
		{
			currentTimeToNextLevel -= GetFrameTime();
		}
		if(transitionToNextLevel && currentTimeToNextLevel < NEXT_LEVEL_MAX_TIME / 2)
		{
			if (currentLevel > levelLoader::CURRENT_LEVELS_COUNT)
			{
				currentLevel = 1;
			}
			levelLoader::loadLevel(currentLevel, bricks_Ptr, BRICKS_COL, BRICKS_ROW, gameScoreRelated::remainingBlocks);
			restartLevel();
			transitionToNextLevel = false;
		}
	}
	void InGame::restartLevel()
	{
		if(gameScoreRelated::currentLifes == 0)
		{
			LostState = true;
			return;
		}
		levelStarted = false;
		pad_Ptr->padToBaseSize();

		for (int i = 0; i < BALL_MAX_AMMOUNT; i++)
		{
			balls_Ptr[i]->active = false;
			balls_Ptr[i]->pos.x = pad_Ptr->rec.x + pad_Ptr->rec.width / 2;
			balls_Ptr[i]->pos.y = pad_Ptr->rec.y - balls_Ptr[i]->getRadius() * 2;
			balls_Ptr[i]->velocity = { 0,0 };
			balls_Ptr[i]->ballToBase();
		}
		balls_Ptr[0]->active = true;

		for (int i = 0; i < gameObjects::POWER_UPS_AMMOUNT; i++)
		{
			power_ups_Ptr[i]->active = false;
		}

	}
	void InGame::pauseLogic()
	{
		ui::events::buttonEventReset();
		upDownGUILogic(GUI_SIZE, GUI_Ptr);
	}
	void InGame::directKeysLogic()
	{

		if (transitionToNextLevel) { return; }

		if (inputRelated::currentInput.down_Left)
		{
			pad_Ptr->move(static_cast<int>((-gameObjects::PAD_BASE_SPEED * GetFrameTime() * screenRelated::SCREEN_WIDTH)));
		}
		else if (inputRelated::currentInput.down_Right)
		{
			pad_Ptr->move(static_cast<int>((gameObjects::PAD_BASE_SPEED * GetFrameTime() * screenRelated::SCREEN_WIDTH)));
		}

		if (!levelStarted && inputRelated::currentInput.enter)
		{
			levelStarted = true;
			balls_Ptr[0]->startBall();
		}

		if (inputRelated::currentInput.pause) { sceneRelated::pause = true; }

#if DEBUG
		if (IsKeyPressed(KEY_H))
		{
			events::multiballPowerUp = true;
		}
		else if (IsKeyPressed(KEY_J))
		{
			events::paddlePowerUp = true;
		}
		else if (IsKeyPressed(KEY_K))
		{
			events::fireballPowerUp = true;
		}
		else if (IsKeyPressed(KEY_L))
		{
			events::ballSpeedPowerUp = true;
		}
		else if(IsKeyPressed(KEY_O))
		{
			gameScoreRelated::remainingBlocks = 0; //Next level
		}
#endif	

	}
	void InGame::nextlevelAnimation()
	{
		if(transitionToNextLevel && currentTimeToNextLevel > NEXT_LEVEL_MAX_TIME / 2 && nextLevelAlphaImage < 255 - NEXT_LEVEL_ANIMATION_SPEED)
		{
			nextLevelAlphaImage += NEXT_LEVEL_ANIMATION_SPEED;
		}
		else if(nextLevelAlphaImage > NEXT_LEVEL_ANIMATION_SPEED)
		{
			nextLevelAlphaImage -= NEXT_LEVEL_ANIMATION_SPEED;
		}
		Color auxColor = { 0, 0, 0, nextLevelAlphaImage };
		Color textAuxColor = { 255, 255, 255, nextLevelAlphaImage };
		DrawTexture(ui::textures::levelChangeTexture, 0, 0, auxColor);
		DrawText(FormatText("LEVEL %i", currentLevel), screenRelated::SCREEN_WIDTH / 2 - MeasureText(FormatText("LEVEL %i", currentLevel), ui::textScreenModifier(NEXT_LEVEL_SIZE))/2, screenRelated::SCREEN_HEIGHT / 2, ui::textScreenModifier(NEXT_LEVEL_SIZE), textAuxColor);
	}
	void InGame::drawPauseUI()
	{
		DrawTexture(ui::textures::backPauseTexture, static_cast<int>(PAUSE_OVERLAY_REC.x), static_cast<int>(PAUSE_OVERLAY_REC.y), WHITE);
		drawUpDownGUI(GUI_Ptr, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);
		Vector2 textSizeAux = MeasureTextEx(ui::textures::myfont, PAUSE_OVERLAY_TEXT, ui::textScreenModifier(PAUSE_OVERLAY_TEXT_SIZE), 1);
		ui::drawTextWithSecondFont(PAUSE_OVERLAY_TEXT, (PAUSE_OVERLAY_REC.x + PAUSE_OVERLAY_REC.width / 2) - textSizeAux.x / 2, PAUSE_OVERLAY_TEXT_POS.y, static_cast<int>(ui::textScreenModifier(PAUSE_OVERLAY_TEXT_SIZE)), WHITE);
	}
	void InGame::drawGameObjects()
	{
		for (int i = 0; i < gameObjects::BRICKS_COL; i++)
		{
			for (int j = 0; j < gameObjects::BRICKS_ROW; j++)
			{
				if (bricks_Ptr[i][j]->active)
				{
					DrawTexture(*bricks_Ptr[i][j]->getTexturePtr(), static_cast<int>(bricks_Ptr[i][j]->rec.x), static_cast<int>(bricks_Ptr[i][j]->rec.y), bricks_Ptr[i][j]->getColor());
				}
			}
		}

		DrawTexture(gameObjects::textures::padTexture, static_cast<int>(pad_Ptr->rec.x), static_cast<int>(pad_Ptr->rec.y), WHITE);

		for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
		{
			if (balls_Ptr[i]->active)
			{
				DrawTexture(*balls_Ptr[i]->getTexture(), static_cast<int>(balls_Ptr[i]->pos.x - balls_Ptr[i]->getRadius()), static_cast<int>(balls_Ptr[i]->pos.y - balls_Ptr[i]->getRadius()), WHITE);
			}
		}

		for (int i = 0; i < gameObjects::POWER_UPS_AMMOUNT; i++)
		{
			if(power_ups_Ptr[i]->active)
			{
				DrawTexture(*power_ups_Ptr[i]->getTexture(), static_cast<int>(power_ups_Ptr[i]->getPosition().x - power_ups_Ptr[i]->getRadius()), static_cast<int>(power_ups_Ptr[i]->getPosition().y - power_ups_Ptr[i]->getRadius()), power_ups_Ptr[i]->getColor());
			}
		}
	}
	void InGame::drawDebug()
	{
		DrawRectangleLines(static_cast<int>(pad_Ptr->rec.x), static_cast<int>(pad_Ptr->rec.y), static_cast<int>(pad_Ptr->rec.width), static_cast<int>(pad_Ptr->rec.height), GREEN);
		for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
		{
			if (balls_Ptr[i]->active)
			{
				DrawCircleLines(static_cast<int>(balls_Ptr[i]->pos.x), static_cast<int>(balls_Ptr[i]->pos.y), balls_Ptr[i]->getRadius(), GREEN);
			}
		}
		for (int i = 0; i < gameObjects::BRICKS_COL; i++)
		{
			for (int j = 0; j < gameObjects::BRICKS_ROW; j++)
			{
				if (bricks_Ptr[i][j]->active)
				{
					DrawRectangleLines(static_cast<int>(bricks_Ptr[i][j]->rec.x), static_cast<int>(bricks_Ptr[i][j]->rec.y), static_cast<int>(bricks_Ptr[i][j]->rec.width), static_cast<int>(bricks_Ptr[i][j]->rec.height), GREEN);
					int aux = bricks_Ptr[i][j]->getCurrentLifes();
					DrawText(FormatText("%i", aux), static_cast<int>(bricks_Ptr[i][j]->rec.x + bricks_Ptr[i][j]->rec.width / 2), static_cast<int>(bricks_Ptr[i][j]->rec.y + bricks_Ptr[i][j]->rec.height / 2), 20, GREEN);
				}
			}
		}
		for (int i = 0; i < POWER_UPS_AMMOUNT; i++)
		{
			if(power_ups_Ptr[i]->active)
			{
				DrawCircleLines(static_cast<int>(power_ups_Ptr[i]->getPosition().x), static_cast<int>(power_ups_Ptr[i]->getPosition().y), power_ups_Ptr[i]->getRadius(), GREEN);
			}
		}
		DrawText("DEBUG COMMANDS: H - MULTIBALL / J - PADDLE SIZE / K - FIREBALL / L SPEED / O NEXT LEVEL", static_cast<int>(screenRelated::SCREEN_WIDTH * 1 / 4.f), static_cast<int>(pad_Ptr->rec.y + pad_Ptr->rec.height * 3 / 2.f), 20, GREEN);

	}
	void InGame::deleteGameObjects()
	{
		delete bound_Ptr;

		delete pad_Ptr;

		for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
		{
			delete balls_Ptr[i];
		}

		for (int i = 0; i < gameObjects::BRICKS_COL; i++)
		{
			for (int j = 0; j < gameObjects::BRICKS_ROW; j++)
			{
				delete bricks_Ptr[i][j];
			}
		}

		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUI_Ptr[i];
		}

		for (int i = 0; i < gameObjects::POWER_UPS_AMMOUNT; i++)
		{
			delete power_ups_Ptr[i];
		}

	}
	void InGame::unloadTextures()
	{

		UnloadTexture(gameObjects::textures::padTexture);
		UnloadTexture(gameObjects::textures::outerBoundsTexture);
		UnloadTexture(gameObjects::textures::ballTexture);
		UnloadTexture(gameObjects::textures::fireball);

		UnloadTexture(gameObjects::textures::commonBrickTexture);
		UnloadTexture(gameObjects::textures::indestructibleBrickTexture);
		UnloadTexture(gameObjects::textures::explosiveBrickTexture);

		UnloadTexture(gameObjects::textures::ballSpeedPowerUp);
		UnloadTexture(gameObjects::textures::fireballPowerUp);
		UnloadTexture(gameObjects::textures::paddlePowerUp);
		UnloadTexture(gameObjects::textures::multiballPowerUp);

		UnloadTexture(ui::textures::backPauseTexture);
	}

	std::string Unpause::getText()
	{
		return derivedText;
	}
	void Unpause::action()
	{
		sceneRelated::pause = false;
	}
	std::string BackToMenuButton::getText()
	{
		return derivedText;
	}
	void BackToMenuButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::MAIN_MENU);
	}

}