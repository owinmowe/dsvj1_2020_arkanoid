#include "change_keys.h"

#include "Arkanoid/ExternalVariables/external_variables.h"
#include "scene_manager.h"
#include "Arkanoid/Audio/audio.h"

namespace change_keys
{

	using namespace ui;

	ChangeKeys::ChangeKeys()
	{
		init();
	}
	ChangeKeys::~ChangeKeys()
	{
		deInit();
	}
	void ChangeKeys::init()
	{

		buttonSize.x = GUI_WIDTH;
		buttonSize.y = GUI_HEIGHT;
		setGUITexturesSize();

		GUIComponent[0] = new UpKeyButton;
		GUIComponent[1] = new DownKeyButton;
		GUIComponent[2] = new LeftKeyButton;
		GUIComponent[3] = new RightKeyButton;
		GUIComponent[4] = new EnterKeyButton;
		GUIComponent[5] = new SpecialKeyButton;
		GUIComponent[6] = new PauseKeyButton;
		GUIComponent[7] = new ResetKeysButton;
		GUIComponent[8] = new BackButton;

		GUIComponent[0]->selected = true;

	}
	void ChangeKeys::deInit()
	{

		textures::menu::buttonTexture.width = static_cast<int>(screenRelated::screenModifier(buttonSize).x);
		textures::menu::buttonTexture.height = static_cast<int>(screenRelated::screenModifier(buttonSize).y);

		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUIComponent[i];
		}

	}
	void ChangeKeys::update()
	{

		if (!modifyingKey)
		{
			keysGUILogic(GUI_SIZE, GUIComponent);
		}
		else
		{
			events::buttonEventReset(); // To stop events from not deactivating if they are outside the modifing key parameter
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUIComponent[i]->selected)
				{
					static_cast<KeyChangeButton*>(GUIComponent[i])->changeKey();
					break;
				}
			}
		}

		directKeysLogic();

		audio::menuAudio(events::moveEvent, events::selectEvent);

	}
	void ChangeKeys::directKeysLogic()
	{

	}
	void ChangeKeys::draw()
	{

		backgroundAnimation();
		drawTextWithSecondFont(TITLE_TEXT, (screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, TITLE_TEXT, textScreenModifier(TITLE_SIZE), 1).x / 2), (screenRelated::SCREEN_HEIGHT * TITLE_POSITION), static_cast<int>(textScreenModifier(TITLE_SIZE)), WHITE);
		drawKeysGUI(GUIComponent, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);
		drawKeysHUD(GUIComponent);

	}
	void ChangeKeys::toggleModifyKey()
	{
		modifyingKey = !modifyingKey;
	}

	void KeyChangeButton::changeKey() { return; }
	std::string KeyChangeButton::getActionText() { return getText(); }
	std::string UpKeyButton::getText() { return returnCurrentKeycodeString(inputRelated::currentKeycodes.pressed_Up); }
	std::string UpKeyButton::getActionText() { return actionText; }
	void UpKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void UpKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.pressed_Up); }
	std::string DownKeyButton::getText() { return returnCurrentKeycodeString(inputRelated::currentKeycodes.pressed_Down); }
	std::string DownKeyButton::getActionText() { return actionText; }
	void DownKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void DownKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.pressed_Down); }
	std::string LeftKeyButton::getText() { return returnCurrentKeycodeString(inputRelated::currentKeycodes.pressed_Left); }
	std::string LeftKeyButton::getActionText() { return actionText; }
	void LeftKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void LeftKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.pressed_Left); }
	std::string RightKeyButton::getText() { return returnCurrentKeycodeString(inputRelated::currentKeycodes.pressed_Right); }
	std::string RightKeyButton::getActionText() { return actionText; }
	void RightKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void RightKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.pressed_Right); }
	std::string EnterKeyButton::getText() { return returnCurrentKeycodeString(inputRelated::currentKeycodes.enter); }
	std::string EnterKeyButton::getActionText() { return actionText; }
	void EnterKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void EnterKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.enter); }
	std::string SpecialKeyButton::getText() { return returnCurrentKeycodeString(inputRelated::currentKeycodes.special); }
	std::string SpecialKeyButton::getActionText() { return actionText; }
	void SpecialKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void SpecialKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.special); }
	std::string PauseKeyButton::getText(){ return returnCurrentKeycodeString(inputRelated::currentKeycodes.pause); }
	std::string PauseKeyButton::getActionText() { return actionText; }
	void PauseKeyButton::action() { static_cast<ChangeKeys*>(sceneRelated::currentScene_Ptr)->toggleModifyKey(); }
	void PauseKeyButton::changeKey() { inputRelated::changeCurrentKeycode(inputRelated::currentKeycodes.pause); }

	std::string ResetKeysButton::getText()
	{
		return derivedText;
	}
	void ResetKeysButton::action()
	{
		inputRelated::resetKeycodes();
	}
	std::string BackButton::getText()
	{
		return derivedText;
	}
	void BackButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::OPTIONS);
	}

	void keysGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{

		events::buttonEventReset();

		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUI_Ptr[i]->selected)
			{
				GUI_Ptr[i]->update();
				break;
			}
		}

		if (inputRelated::currentInput.pressed_Down)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i + 1 < GUI_SIZE && (i < 1 || i > 3))
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i + 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i == 1)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[4]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && (i == 3 || i == 2))
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i == GUI_SIZE - 1)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
		else if (inputRelated::currentInput.pressed_Up)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i - 1 >= 0 && (i < 2 || i > 4))
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i - 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i == 4)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && (i == 3 || i == 2))
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i == 0)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[GUI_SIZE - 1]->selected = true;
					break;
				}
			}
		}
		else if(inputRelated::currentInput.pressed_Left)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && (i == 0 || i == 1))
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[2]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i == 3)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
		else if (inputRelated::currentInput.pressed_Right)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && (i == 0 || i == 1))
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[3]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i == 2)
				{
					events::moveEvent = true;
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
	}
	void drawKeysGUI(GUIcomponent* GUI_Ptr[], const int GUI_SIZE, const float GUI_STARTING_POSITION, const float GUI_SEPARATION)
	{

		GUI_Ptr[0]->drawComponent(GUI_MIDDLE_X, GUI_STARTING_POSITION);
		GUI_Ptr[1]->drawComponent(GUI_MIDDLE_X, GUI_STARTING_POSITION + GUI_SEPARATION);
		GUI_Ptr[2]->drawComponent(GUI_LEFT_X, GUI_STARTING_POSITION + GUI_SEPARATION / 2);
		GUI_Ptr[3]->drawComponent(GUI_RIGHT_X, GUI_STARTING_POSITION + GUI_SEPARATION / 2);

		for (int i = 4; i < GUI_SIZE - 1; i++)
		{
			GUI_Ptr[i]->drawComponent(GUI_MIDDLE_X, GUI_STARTING_POSITION + GUI_SEPARATION * (i - 2));
		}

		GUI_Ptr[GUI_SIZE - 1]->drawComponent(GUI_HALFSCREEN, GUI_STARTING_POSITION + GUI_SEPARATION * (GUI_SIZE - 3));

	}
	void drawKeysHUD(GUIcomponent* GUI_Ptr[])
	{
		char auxText[20];
		strcpy_s(auxText, static_cast<KeyChangeButton*>(GUI_Ptr[0])->getActionText().c_str());
		Vector2 newPos;
		newPos.x = GUI_MIDDLE_X * screenRelated::SCREEN_WIDTH + screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		newPos.y = GUI_STARTING_POSITION * screenRelated::SCREEN_HEIGHT - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).y / 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);

		strcpy_s(auxText, static_cast<KeyChangeButton*>(GUI_Ptr[1])->getActionText().c_str());
		newPos.y += GUI_SEPARATION * screenRelated::SCREEN_HEIGHT;
		newPos.x = GUI_MIDDLE_X * screenRelated::SCREEN_WIDTH + screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);
		
		strcpy_s(auxText, static_cast<KeyChangeButton*>(GUI_Ptr[2])->getActionText().c_str());
		newPos.y -= GUI_SEPARATION * screenRelated::SCREEN_HEIGHT / 2;
		newPos.x = GUI_LEFT_X * screenRelated::SCREEN_WIDTH + screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);
		
		strcpy_s(auxText, static_cast<KeyChangeButton*>(GUI_Ptr[3])->getActionText().c_str());
		newPos.x = GUI_RIGHT_X * screenRelated::SCREEN_WIDTH + screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
		DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);


		for (int i = 4; i < GUI_SIZE - 2; i++)
		{

			strcpy_s(auxText, static_cast<KeyChangeButton*>(GUI_Ptr[i])->getActionText().c_str());
			newPos.x = GUI_MIDDLE_X * screenRelated::SCREEN_WIDTH + screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2;
			newPos.y = GUI_STARTING_POSITION * screenRelated::SCREEN_HEIGHT + GUI_SEPARATION * screenRelated::SCREEN_HEIGHT * (i - 2) - MeasureTextEx(textures::myfont, auxText, textScreenModifier(TEXT_SIZE_SCALE), 1).y / 2;
			DrawTextEx(textures::myfont, auxText, newPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);

		}

	}
	std::string returnCurrentKeycodeString(int keycode)
	{
		switch (keycode)
		{
		case KEY_UP:
			return "Up Arrow";
			break;
		case KEY_DOWN:
			return "Down Arrow";
			break;
		case KEY_LEFT:
			return "Left Arrow";
			break;
		case KEY_RIGHT:
			return "Right Arrow";
			break;
		case KEY_ENTER:
			return "Enter Key";
			break;
		case KEY_SPACE:
			return "Space Key";
			break;
		default:
			char aux = keycode;
			aux = toupper(aux);
			std::string aux2(1, aux);
			return aux2;
			break;
		}
	}

}