#include "options.h"

#include "Arkanoid/ExternalVariables/external_variables.h"
#include "scene_manager.h"
#include "Arkanoid/Audio/audio.h"

namespace options
{

	using namespace ui;

	Options::Options()
	{
		init();
	}
	Options::~Options()
	{
		deInit();
	}
	void Options::init()
	{
		buttonSize.x = BUTTON_WIDTH;
		buttonSize.y = BUTTON_HEIGHT;
		SliderSize.x = SLIDER_WIDTH;
		SliderSize.y = SLIDER_HEIGHT;
		setGUITexturesSize();

		GUIComponent[0] = new SoundVolumeSlider;
		GUIComponent[1] = new MusicVolumeSlider;
		GUIComponent[2] = new ChangeKeysButton;
		GUIComponent[3] = new FullScreenButton;
		GUIComponent[4] = new BackButton;

		GUIComponent[0]->selected = true;

	}
	void Options::deInit()
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUIComponent[i];
		}

	}
	void Options::update()
	{

		upDownGUILogic(GUI_SIZE, GUIComponent);
		directKeysLogic();
		audio::menuAudio(events::moveEvent, events::selectEvent);

	}
	void Options::directKeysLogic()
	{

	}
	void Options::draw()
	{

		backgroundAnimation();
		drawTextWithSecondFont(TITLE_TEXT, (screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, TITLE_TEXT, textScreenModifier(TITLE_SIZE), 1).x / 2), (screenRelated::SCREEN_HEIGHT * TITLE_POSITION), static_cast<int>(textScreenModifier(TITLE_SIZE)), WHITE);
		drawUpDownGUI(GUIComponent, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);

	}

	SoundVolumeSlider::SoundVolumeSlider()
	{
		handlePosition = static_cast<int>(audio::soundVolume * 10);
	}
	std::string SoundVolumeSlider::getText()
	{
		return derivedText;
	}
	void SoundVolumeSlider::leftAction()
	{
		audio::soundVolume -= .1f;
		audio::setAllSoundVolume();
	}
	void SoundVolumeSlider::rightAction()
	{
		audio::soundVolume += .1f;
		audio::setAllSoundVolume();
	}
	MusicVolumeSlider::MusicVolumeSlider()
	{
		handlePosition = static_cast<int>(audio::musicVolume * 10);
	}
	std::string MusicVolumeSlider::getText()
	{
		return derivedText;
	}
	void MusicVolumeSlider::leftAction()
	{
		audio::musicVolume -= .1f;
		audio::setAllMusicVolume();
	}
	void MusicVolumeSlider::rightAction()
	{
		audio::musicVolume += .1f;
		audio::setAllMusicVolume();
	}
	std::string ChangeKeysButton::getText()
	{
		return derivedText;
	}
	void ChangeKeysButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::CHANGE_KEYS);
	}
	std::string FullScreenButton::getText()
	{
		std::string aux;
		IsWindowFullscreen() ? aux = derivedTextFullScreen : aux = derivedTextWindow;
		return aux;
	}
	void FullScreenButton::action()
	{
		ToggleFullscreen();
	}
	std::string BackButton::getText()
	{
		return derivedText;
	}
	void BackButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::MAIN_MENU);
	}

}