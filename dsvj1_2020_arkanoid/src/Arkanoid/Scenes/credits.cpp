#include "credits.h"
#include "raylib.h"
#include "Arkanoid/ExternalVariables/external_variables.h"
#include "scene_manager.h"
#include "Arkanoid/Audio/audio.h"

namespace credits
{

	using namespace ui;

	Credits::Credits()
	{
		init();
	}
	Credits::~Credits()
	{
		deInit();
	}
	void Credits::init()
	{

		buttonSize = { GUI_WIDTH, GUI_HEIGHT };
		Vector2 logoSize = screenRelated::screenModifier({ 1 / 8.f, 1 / 8.f });
		textures::menu::raylibLogoTexture.width = static_cast<int>(logoSize.x);
		textures::menu::raylibLogoTexture.height = static_cast<int>(logoSize.y);
		setGUITexturesSize();

		GUI_Ptr[0] = new BackButton;
		GUI_Ptr[0]->selected = true;

	}
	void Credits::deInit()
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUI_Ptr[i];
		}

	}
	void Credits::update()
	{

		upDownGUILogic(GUI_SIZE, GUI_Ptr);
		directKeysLogic();
		audio::menuAudio(events::moveEvent, events::selectEvent);

	}
	void Credits::directKeysLogic()
	{

	}
	void Credits::draw()
	{
		
		backgroundAnimation();
		Vector2 textPos = { (screenRelated::SCREEN_WIDTH * MADE_BY_TEXT_POS.x + MeasureTextEx(textures::myfont, MADE_BY_TEXT, textScreenModifier(TEXT_SIZE_SCALE), 1).x / 2), (screenRelated::SCREEN_HEIGHT * MADE_BY_TEXT_POS.y) };
		DrawTextEx(textures::myfont, MADE_BY_TEXT, textPos, textScreenModifier(TEXT_SIZE_SCALE), 1, WHITE);
		DrawTexture(textures::menu::raylibLogoTexture, static_cast<int>((screenRelated::SCREEN_WIDTH * LOGO_POSITON_POS.x - textures::menu::raylibLogoTexture.width / 2)), static_cast<int>((screenRelated::SCREEN_HEIGHT * LOGO_POSITON_POS.y) - textures::menu::raylibLogoTexture.height / 2), WHITE);
		drawUpDownGUI(GUI_Ptr, GUI_SIZE, BUTTONS_STARTING_POSITION, BUTTONS_SEPARATION);

	}
	std::string BackButton::getText()
	{
		return derivedText;
	}
	void BackButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::MAIN_MENU);
	}
}