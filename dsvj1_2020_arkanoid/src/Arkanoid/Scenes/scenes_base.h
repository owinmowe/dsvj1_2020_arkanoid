#ifndef SCENES_BASE_H
#define SCENES_BASE_H

class SceneBase
{

private:
	virtual void init() = 0;
	virtual void deInit() = 0;

public:
	virtual void update() = 0;
	virtual void draw() = 0;

};

#endif SCENES_BASE_H
