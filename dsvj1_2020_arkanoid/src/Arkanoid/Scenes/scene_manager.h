#ifndef SCENES_MANAGER_H
#define SCENES_MANAGER_H
#include "scenes_base.h"

namespace sceneRelated
{

	enum class Scene { MAIN_MENU, OPTIONS, CHANGE_KEYS, CREDITS, IN_GAME };
	extern SceneBase* currentScene_Ptr;
	extern bool playing;
	extern bool pause;

	void ChangeSceneTo(sceneRelated::Scene nextScene);

}

#endif SCENES_MANAGER_H