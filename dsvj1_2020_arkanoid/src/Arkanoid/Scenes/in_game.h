#ifndef IN_GAME_H
#define IN_GAME_H

#include "scenes_base.h"
#include "Arkanoid/gameObjects/game_objects.h"
#include "Arkanoid/UI/ui.h"

namespace in_game
{

	const int GUI_SIZE = 2;
	const float GUI_STARTING_POSITION = 1 / 3.f;
	const float GUI_SEPARATION = 1 / 6.f;

	const int LIFES_PER_GAME = 5;

	class InGame : public SceneBase
	{

	public:
		InGame();
		~InGame();
		void update() override;
		void lostScreenLogic();
		void levelLogic();
		void powerUpsLogic();
		void draw() override;

		void nextlevelAnimation();

	private:
		void init() override;
		void setGUI();
		void setGameObjects();
		void loadTextures();
		void ballsLogic();
		void restartLevel();
		void pauseLogic();
		void drawPauseUI();
		void drawGameObjects();
		void drawDebug();
		void deInit() override;
		void deleteGameObjects();
		void unloadTextures();
		void directKeysLogic();
		bool levelStarted = false;

		ui::GUIcomponent* GUI_Ptr[GUI_SIZE];
		gameObjects::Brick* bricks_Ptr[gameObjects::BRICKS_COL][gameObjects::BRICKS_ROW];
		gameObjects::Ball* balls_Ptr[gameObjects::BALL_MAX_AMMOUNT];
		gameObjects::Pad* pad_Ptr;
		gameObjects::Bound* bound_Ptr;
		gameObjects::PowerUp* power_ups_Ptr[gameObjects::POWER_UPS_AMMOUNT];

		int currentLevel = 1;
		int nextLevelAlphaImage = 255;
		float currentTimeToNextLevel = 2;
		bool transitionToNextLevel = false;

		bool LostState = false;

	};

	const float NEXT_LEVEL_SIZE = 1/32.f;
	const int NEXT_LEVEL_MAX_TIME = 3;
	const int NEXT_LEVEL_ANIMATION_SPEED = 4;

	const char OVERLAY_PATH_FILE[] = "res/assets/texture/menu/overlayGame.png";

	const char PAUSE_OVERLAY_PATH_FILE[] = "res/assets/texture/menu/overlayPauseMenu.png";
	const Rectangle PAUSE_OVERLAY_REC{ screenRelated::SCREEN_WIDTH * 1 / 3 , screenRelated::SCREEN_HEIGHT * 5 / 24, screenRelated::SCREEN_WIDTH * 1 / 3, screenRelated::SCREEN_HEIGHT * 1 / 2 };
	const char PAUSE_OVERLAY_TEXT[] = "Pause Menu";
	const Vector2 PAUSE_OVERLAY_TEXT_POS{ PAUSE_OVERLAY_REC.x , PAUSE_OVERLAY_REC.y - screenRelated::SCREEN_HEIGHT * 1 / 150 };
	const float PAUSE_OVERLAY_TEXT_SIZE = 1 / 25.f;

	class Unpause : public ui::Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Resume";
	};
	class BackToMenuButton : public ui::Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Go to menu";
	};

}

#endif IN_GAME_H
