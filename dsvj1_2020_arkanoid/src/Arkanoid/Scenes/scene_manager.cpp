#include "scene_manager.h"
#include "Arkanoid/Scenes/in_game.h"
#include "Arkanoid/Scenes/credits.h"
#include "Arkanoid/Scenes/main_menu.h"
#include "Arkanoid/Scenes/options.h"
#include "Arkanoid/Scenes/change_keys.h"

namespace sceneRelated 
{

	bool playing = false;

	bool pause = false;

	SceneBase* currentScene_Ptr;

	void ChangeSceneTo(sceneRelated::Scene nextScene)
	{

		delete sceneRelated::currentScene_Ptr;

		switch (nextScene)
		{
		case sceneRelated::Scene::MAIN_MENU:
			sceneRelated::currentScene_Ptr = new main_menu::MainMenu;
			break;
		case sceneRelated::Scene::OPTIONS:
			sceneRelated::currentScene_Ptr = new options::Options;
			break;
		case sceneRelated::Scene::CHANGE_KEYS:
			sceneRelated::currentScene_Ptr = new change_keys::ChangeKeys;
			break;
		case sceneRelated::Scene::CREDITS:
			sceneRelated::currentScene_Ptr = new credits::Credits;
			break;
		case sceneRelated::Scene::IN_GAME:
			sceneRelated::currentScene_Ptr = new in_game::InGame;
			break;
		default:
			break;
		}

	}
}