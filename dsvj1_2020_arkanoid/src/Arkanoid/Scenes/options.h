#ifndef OPTIONS_H
#define OPTIONS_H

#include "scenes_base.h"
#include "raylib.h"
#include "Arkanoid/UI/ui.h"

namespace options
{

	using namespace ui;

	const char TITLE_TEXT[] = "OPTIONS";
	const float TITLE_SIZE = 1 / 15.f;
	const float TITLE_POSITION = 1 / 16.f;

	const int GUI_SIZE = 5;
	const float BUTTON_WIDTH = 1 / 6.f;
	const float BUTTON_HEIGHT = 1 / 10.f;
	const float SLIDER_WIDTH = 1 / 6.f;
	const float SLIDER_HEIGHT = 1 / 40.f;
	const float GUI_STARTING_POSITION = 1 / 3.f;
	const float GUI_SEPARATION = 1 / 8.f;

	class Options : public SceneBase
	{

	private:
		void init() override;
		void deInit() override;
		GUIcomponent* GUIComponent[GUI_SIZE];

	public:
		Options();
		~Options();
		void update() override;
		void directKeysLogic();
		void draw() override;

	};

	class SoundVolumeSlider : public Slider
	{
	public:
		SoundVolumeSlider();
		bool selected = false;
		std::string getText() override;
		void leftAction() override;
		void rightAction() override;

	private:
		std::string derivedText = "Sound Volume";
	};

	class MusicVolumeSlider : public Slider
	{
	public:
		MusicVolumeSlider();
		bool selected = false;
		std::string getText() override;
		void leftAction() override;
		void rightAction() override;

	private:
		std::string derivedText = "Music Volume";
	};

	class ChangeKeysButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Change Keys";
	};

	class FullScreenButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedTextFullScreen = "Window Mode";
		std::string derivedTextWindow = "Fullscreen Mode";
	};

	class BackButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Back";
	};

}

#endif OPTIONS_H
