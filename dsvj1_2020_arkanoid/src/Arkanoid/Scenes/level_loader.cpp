#include "level_loader.h"
#include "Arkanoid/ExternalVariables/external_variables.h"

namespace levelLoader
{

	void loadLevel(int level, Brick* bricks_Ptr[][BRICKS_ROW], const int BRICKS_COL, const int BRICKS_ROW, int& remainingBlocks)
	{

		level--;
		for (int i = 0; i < BRICKS_COL; i++)
		{
			for (int j = 0; j < BRICKS_ROW; j++)
			{

				BRICKTYPE aux = currentLevels::level[level][i][j];

				switch (aux)
				{
				case levelLoader::BRICKTYPE::NONE:
					bricks_Ptr[i][j] = new Brick;
					break;
				case levelLoader::BRICKTYPE::INDESTRUCTIBLE:
					bricks_Ptr[i][j] = new IndestructibleBrick;
					bricks_Ptr[i][j]->active = true;
					break;
				case levelLoader::BRICKTYPE::EXPLOSIVE:
					bricks_Ptr[i][j] = new ExplosiveBrick;
					bricks_Ptr[i][j]->active = true;
					remainingBlocks++;
					break;
				case levelLoader::BRICKTYPE::COMMON1:
				case levelLoader::BRICKTYPE::COMMON2:
				case levelLoader::BRICKTYPE::COMMON3:
				case levelLoader::BRICKTYPE::COMMON4:
				case levelLoader::BRICKTYPE::COMMON5:
				case levelLoader::BRICKTYPE::COMMON6:
				case levelLoader::BRICKTYPE::COMMON7:
					bricks_Ptr[i][j] = new CommonBrick;
					static_cast<gameObjects::CommonBrick*>(bricks_Ptr[i][j])->setLifes(static_cast<int>(aux) - 2);
					bricks_Ptr[i][j]->active = true;
					remainingBlocks++;
					break;
				}

				bricks_Ptr[i][j]->rec = { screenRelated::SCREEN_WIDTH * gameObjects::BRICKS_STARTING_POSITION, gameObjects::BRICKS_STARTING_POSITION, bricks_Ptr[i][j]->rec.width, bricks_Ptr[i][j]->rec.height };
				bricks_Ptr[i][j]->rec.x += i * bricks_Ptr[i][j]->rec.width + i * gameObjects::BRICKS_SEPARATION + bricks_Ptr[i][j]->rec.width;
				bricks_Ptr[i][j]->rec.y += j * bricks_Ptr[i][j]->rec.height + j * gameObjects::BRICKS_SEPARATION + bricks_Ptr[i][j]->rec.height;

			}
		}

	}
}
