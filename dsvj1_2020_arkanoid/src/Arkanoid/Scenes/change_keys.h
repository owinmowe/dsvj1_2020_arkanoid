#ifndef CHANGE_KEYS_H
#define CHANGE_KEYS_H

#include "scenes_base.h"
#include "raylib.h"
#include "Arkanoid/UI/ui.h"

namespace change_keys
{

	using namespace ui;

	const char TITLE_TEXT[] = "CURRENT KEYS";
	const float TITLE_SIZE = 1 / 20.f;
	const float TITLE_POSITION = 1 / 32.f;

	const int GUI_SIZE = 9;
	const float GUI_WIDTH = 1 / 8.f;
	const float GUI_HEIGHT = 1 / 12.f;
	const float GUI_STARTING_POSITION = 3 / 16.f;
	const float GUI_SEPARATION = 1 / 8.f;
	const float GUI_LEFT_X = 1/ 8.f;
	const float GUI_MIDDLE_X = 1 / 4.f;
	const float GUI_RIGHT_X = 3 / 8.f;
	const float GUI_HALFSCREEN = 1 / 2.f;

	class ChangeKeys : public SceneBase
	{

	public:
		ChangeKeys();
		~ChangeKeys();
		void update() override;
		void directKeysLogic();
		void draw() override;
		void toggleModifyKey();

	private:
		void init() override;
		void deInit() override;
		GUIcomponent* GUIComponent[GUI_SIZE];
		bool modifyingKey = false;

	};

	class KeyChangeButton : public Button
	{
	public:
		virtual void changeKey();
		virtual std::string getActionText();
	private:
	};

	class UpKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		const std::string actionText = "Up";
	};

	class DownKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		const std::string actionText = "Down";
	};

	class LeftKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		const std::string actionText = "Left";
	};

	class RightKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		std::string actionText = "Right";
	};

	class SpecialKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		std::string actionText = "Special";
	};

	class EnterKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		std::string actionText = "Enter";
	};

	class PauseKeyButton : public KeyChangeButton
	{
	public:
		std::string getText() override;
		void action() override;
		void changeKey() override;
		std::string getActionText() override;

	private:
		std::string actionText = "Pause";
	};

	class ResetKeysButton : public Button
	{
	public:
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Reset Keys";
	};

	class BackButton : public Button
	{
	public:
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Back";
	};

	std::string returnCurrentKeycodeString(int keycode);
	void keysGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void drawKeysGUI(GUIcomponent* GUI_Ptr[], const int GUI_SIZE, const float GUI_STARTING_POSITION, const float GUI_SEPARATION);
	void drawKeysHUD(GUIcomponent* GUI_Ptr[]);
}

#endif CHANGE_KEYS_H
