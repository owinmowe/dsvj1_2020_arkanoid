#ifndef CREDITS_H
#define CREDITS_H

#include "scenes_base.h"
#include "raylib.h"
#include "Arkanoid/UI/ui.h"

namespace credits
{

	using namespace ui;

	const int GUI_SIZE = 1;
	const float GUI_WIDTH = 2 / 18.f;
	const float GUI_HEIGHT = 2 / 20.f;

	class Credits : public SceneBase
	{

	private:
		void init() override;
		void deInit() override;
		GUIcomponent* GUI_Ptr[GUI_SIZE];

	public:
		Credits();
		~Credits();
		void update() override;
		void directKeysLogic();
		void draw() override;

	};

	class BackButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Back";
	};

	const char MADE_BY_TEXT[] = "Made by Adrian Sgro in C++ with";
	const Vector2 MADE_BY_TEXT_POS{ 1 / 8.f, 1 / 8.f };
	const Vector2 LOGO_POSITON_POS{ 11 / 16.f, 1 / 8.f };

	const float BUTTONS_STARTING_POSITION = 7 / 8.f;
	const float BUTTONS_SEPARATION = 1 / 8.f;

}

#endif CREDITS_H