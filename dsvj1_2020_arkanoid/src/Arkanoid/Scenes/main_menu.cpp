#include "main_menu.h"

#include "Arkanoid/ExternalVariables/external_variables.h"
#include "scene_manager.h"
#include "Arkanoid/Audio/audio.h"

namespace main_menu
{

	using namespace ui;

	MainMenu::MainMenu()
	{
		init();
	}
	MainMenu::~MainMenu()
	{
		deInit();
	}
	void MainMenu::init()
	{

		textures::backgroundTexture.width = screenRelated::SCREEN_WIDTH;
		textures::backgroundTexture.height = screenRelated::SCREEN_HEIGHT;
		buttonSize = { GUI_WIDTH, GUI_HEIGHT };
		setGUITexturesSize();

		GUI_Ptr[0] = new PlayButton;
		GUI_Ptr[1] = new OptionsButton;
		GUI_Ptr[2] = new CreditsButton;
		GUI_Ptr[3] = new ExitButton;
		GUI_Ptr[0]->selected = true;

		PlayMusicStream(audio::musicVariables.MenuMusic);

	}
	void MainMenu::deInit()
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUI_Ptr[i];
		}

	}
	void MainMenu::update()
	{

		upDownGUILogic(GUI_SIZE, GUI_Ptr);
		directKeysLogic();
		audio::menuAudio(events::moveEvent, events::selectEvent);

	}
	void MainMenu::directKeysLogic()
	{

	}
	void MainMenu::draw()
	{

		backgroundAnimation();
		drawTextWithSecondFont(TITLE_TEXT, (screenRelated::SCREEN_WIDTH / 2 - MeasureTextEx(textures::myfont, TITLE_TEXT, textScreenModifier(TITLE_SIZE), 1).x / 2), (screenRelated::SCREEN_HEIGHT * TITLE_POSITION), static_cast<int>(textScreenModifier(TITLE_SIZE)), WHITE);
		drawUpDownGUI(GUI_Ptr, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);

	}

	std::string PlayButton::getText()
	{
		return derivedText;
	}
	void PlayButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::IN_GAME);
	}
	std::string OptionsButton::getText()
	{
		return derivedText;
	}
	void OptionsButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::OPTIONS);
	}
	std::string CreditsButton::getText()
	{
		return derivedText;
	}
	void CreditsButton::action()
	{
		sceneRelated::ChangeSceneTo(sceneRelated::Scene::CREDITS);
	}
	std::string ExitButton::getText()
	{
		return derivedText;
	}
	void ExitButton::action()
	{
		sceneRelated::playing = false;
		events::selectEvent = false;
	}

}