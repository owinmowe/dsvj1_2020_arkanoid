#ifndef COLLISION_H
#define COLLISION_H
#include "Arkanoid/gameObjects/game_objects.h"

using namespace gameObjects;

void collisionDetection(Pad* pad_Ptr, PowerUp* power_Ups_Ptr[], const int POWER_UPS_AMMOUNT, Ball* balls_Ptr[], Brick* bricks_Ptr[][BRICKS_ROW], const int BRICKS_COL, const int BRICKS_ROW, Bound* bound_Ptr);

#endif COLLISION_H