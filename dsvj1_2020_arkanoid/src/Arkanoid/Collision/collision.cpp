#include "collision.h"
#include "raylib.h"
#include <math.h>

void padBoundCollision(Pad* pad_Ptr, Bound* outerBound_Ptr)
{
	if (pad_Ptr->rec.x < outerBound_Ptr->rec.x + outerBound_Ptr->boundSize.x)
	{
		pad_Ptr->rec.x = outerBound_Ptr->rec.x + outerBound_Ptr->boundSize.x;
	}
	else if (pad_Ptr->rec.x + pad_Ptr->rec.width > outerBound_Ptr->rec.x + outerBound_Ptr->rec.width - outerBound_Ptr->boundSize.x)
	{
		pad_Ptr->rec.x = outerBound_Ptr->rec.x + outerBound_Ptr->rec.width - outerBound_Ptr->boundSize.x - pad_Ptr->rec.width;
	}
}

void ballPadCollision(Pad* pad_Ptr, Ball* balls_Ptr[], int i)
{
	if (CheckCollisionCircleRec(balls_Ptr[i]->pos, balls_Ptr[i]->getRadius(), { pad_Ptr->rec.x, pad_Ptr->rec.y, pad_Ptr->rec.width, pad_Ptr->rec.height }))
	{
		if (balls_Ptr[i]->getPadCollision()) { return; }
		balls_Ptr[i]->velocity.y = -balls_Ptr[i]->velocity.y;
		balls_Ptr[i]->velocity.x = (balls_Ptr[i]->pos.x - (pad_Ptr->rec.x + pad_Ptr->rec.width / 2)) / (pad_Ptr->rec.width / 2) * BALL_BASE_SPEED * balls_Ptr[i]->getSpeedModifier() * screenRelated::SCREEN_WIDTH;
		pad_Ptr->onCollision();
		balls_Ptr[i]->setPadCollision(true);
	}
	else
	{
		balls_Ptr[i]->setPadCollision(false);
	}
}

void ballBoundsCollision(Ball* balls_Ptr[], int i, Bound* outerBound_Ptr)
{
	if (balls_Ptr[i]->pos.x < outerBound_Ptr->rec.x + outerBound_Ptr->boundSize.x)
	{
		if(balls_Ptr[i]->velocity.x < 0)
		{
			balls_Ptr[i]->velocity.x = -balls_Ptr[i]->velocity.x;
		}
		outerBound_Ptr->onCollision();
	}
	else if(balls_Ptr[i]->pos.x > outerBound_Ptr->rec.x + outerBound_Ptr->rec.width - outerBound_Ptr->boundSize.x)
	{
		if (balls_Ptr[i]->velocity.x > 0)
		{
			balls_Ptr[i]->velocity.x = -balls_Ptr[i]->velocity.x;
		}
		outerBound_Ptr->onCollision();
	}
	else if (balls_Ptr[i]->pos.y < outerBound_Ptr->rec.y + outerBound_Ptr->boundSize.y)
	{
		if (balls_Ptr[i]->velocity.y < 0)
		{
			balls_Ptr[i]->velocity.y = -balls_Ptr[i]->velocity.y;
		}
		outerBound_Ptr->onCollision();
	}
	else if(balls_Ptr[i]->pos.y > outerBound_Ptr->rec.y + outerBound_Ptr->rec.height - outerBound_Ptr->boundSize.y)
	{
		if (balls_Ptr[i]->velocity.y > 0)
		{
			balls_Ptr[i]->velocity.y = -balls_Ptr[i]->velocity.y;
		}
		outerBound_Ptr->onCollision();
	}
}

void ballBricksCollision(Ball* balls_Ptr[], int i, Brick* bricks_Ptr[][BRICKS_ROW], const int BRICKS_COL, const int BRICKS_ROW)
{
	for (int j = 0; j < BRICKS_COL; j++)
	{
		for (int k = 0; k < BRICKS_ROW; k++)
		{
			if (bricks_Ptr[j][k]->active)
			{
				if (CheckCollisionCircleRec(balls_Ptr[i]->pos, balls_Ptr[i]->getRadius(), bricks_Ptr[j][k]->rec))
				{

					bool reCalculateCollision = false;
					Brick* lastBrickCollision_Ptr = bricks_Ptr[j][k];
					do
					{
						if(balls_Ptr[i]->pos.x > balls_Ptr[i]->previousPos.x)
						{
							balls_Ptr[i]->pos.x -= GetFrameTime();
						}
						else if(balls_Ptr[i]->pos.x < balls_Ptr[i]->previousPos.x)
						{
							balls_Ptr[i]->pos.x += GetFrameTime();
						}

						if (balls_Ptr[i]->pos.y > balls_Ptr[i]->previousPos.y)
						{
							balls_Ptr[i]->pos.y -= GetFrameTime();
						}
						else if (balls_Ptr[i]->pos.y < balls_Ptr[i]->previousPos.y)
						{
							balls_Ptr[i]->pos.y += GetFrameTime();
						}

						reCalculateCollision = false;

						for (int l = 0; l < BRICKS_COL; l++)
						{
							for (int m = 0; m < BRICKS_ROW; m++)
							{
								if (bricks_Ptr[l][m]->active && CheckCollisionCircleRec(balls_Ptr[i]->pos, balls_Ptr[i]->getRadius(), bricks_Ptr[l][m]->rec))
								{
									lastBrickCollision_Ptr = bricks_Ptr[l][m];
									reCalculateCollision = true;
									break;
								}
							}
							if (reCalculateCollision) { break; }
						}
					} while (reCalculateCollision);

					int px = static_cast<int>(balls_Ptr[i]->pos.x);
					if (px < lastBrickCollision_Ptr->rec.x)
					{
						px = static_cast<int>(lastBrickCollision_Ptr->rec.x);
					}
					if (px > lastBrickCollision_Ptr->rec.x + lastBrickCollision_Ptr->rec.width)
					{
						px = static_cast<int>(lastBrickCollision_Ptr->rec.x + lastBrickCollision_Ptr->rec.width);
					}

					int py = static_cast<int>(balls_Ptr[i]->pos.y);
					if (py < lastBrickCollision_Ptr->rec.y)
					{
						py = static_cast<int>(lastBrickCollision_Ptr->rec.y);
					}
					if (py > lastBrickCollision_Ptr->rec.y + lastBrickCollision_Ptr->rec.height)
					{
						py = static_cast<int>(lastBrickCollision_Ptr->rec.y + lastBrickCollision_Ptr->rec.height);
					}

					if(!balls_Ptr[i]->isFireball())
					{
						if (px == static_cast<int>(lastBrickCollision_Ptr->rec.x) || px == static_cast<int>(lastBrickCollision_Ptr->rec.x + lastBrickCollision_Ptr->rec.width))
						{
							balls_Ptr[i]->invertVelocityX();
						}
						else
						{
							balls_Ptr[i]->invertVelocityY();
						}
					}
					lastBrickCollision_Ptr->onCollision(balls_Ptr[i]->isFireball());
				}
			}
		}
	}
}

void padPowerUpDetection(Pad* pad_Ptr, PowerUp* power_Ups_Ptr[], const int POWER_UPS_AMMOUNT)
{
	for (int i = 0; i < POWER_UPS_AMMOUNT; i++)
	{
		if(power_Ups_Ptr[i]->active && CheckCollisionCircleRec(power_Ups_Ptr[i]->getPosition(), power_Ups_Ptr[i]->getRadius(), pad_Ptr->rec))
		{
			power_Ups_Ptr[i]->onCollision();
		}
	}
}

void collisionDetection(Pad* pad_Ptr, PowerUp* power_Ups_Ptr[], const int POWER_UPS_AMMOUNT, Ball* balls_Ptr[], Brick* bricks_Ptr[][BRICKS_ROW], const int BRICKS_COL, const int BRICKS_ROW, Bound* bound_Ptr)
{

	padBoundCollision(pad_Ptr, bound_Ptr);

	padPowerUpDetection(pad_Ptr, power_Ups_Ptr, POWER_UPS_AMMOUNT);

	for (int i = 0; i < gameObjects::BALL_MAX_AMMOUNT; i++)
	{
		if (balls_Ptr[i]->active)
		{

			ballBoundsCollision(balls_Ptr, i, bound_Ptr);

			ballPadCollision(pad_Ptr, balls_Ptr, i);

			ballBricksCollision(balls_Ptr, i, bricks_Ptr, BRICKS_COL, BRICKS_ROW);

		}
	}

}

