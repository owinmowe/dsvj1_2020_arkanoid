#ifndef ARKANOID_H
#define ARKANOID_H

class Arkanoid {

private:
	void input();
	void update();
	void draw();

public:
	Arkanoid();
	~Arkanoid();
	void play();

};

#endif ARKANOID_H