#include "arkanoid.h"

#include "raylib.h"

#include "ExternalVariables/external_variables.h"
#include "Input/input.h"
#include "Update/update.h"
#include "Render/draw.h"
#include "Scenes/scene_manager.h"
#include "UI/ui.h"
#include "Audio/audio.h"

Arkanoid::Arkanoid()
{

    using namespace ui;

    InitWindow(screenRelated::SCREEN_WIDTH, screenRelated::SCREEN_HEIGHT, "Space Arkanoid");
    InitAudioDevice();
    SetTargetFPS(screenRelated::FRAMES_PER_SECOND);
    SetExitKey(0);

    textures::backgroundTexture = LoadTexture(BACKGROUND_PATH_FILE);
    textures::menu::buttonTexture = LoadTexture(BUTTON_PATH_FILE);
    textures::menu::sliderBarTexture = LoadTexture(SLIDER_BAR_PATH_FILE);
    textures::menu::sliderHandleTexture = LoadTexture(SLIDER_HANDLE_PATH_FILE);
    textures::menu::raylibLogoTexture = LoadTexture(RAYLIBLOGO_PATH_FILE);

    textures::myfont = LoadFontEx(FONT_PATH_FILE, 200, 0, NULL);

    audio::loadSoundsInMemory(audio::soundsVariables);
    audio::setAllSoundVolume();
    audio::loadMusicInMemory(audio::musicVariables);
    audio::setAllMusicVolume();

    sceneRelated::playing = true;
    sceneRelated::ChangeSceneTo(sceneRelated::Scene::MAIN_MENU);

    HideCursor();

}
Arkanoid::~Arkanoid()
{
    
    using namespace ui;

    UnloadTexture(textures::backgroundTexture);
    UnloadTexture(textures::menu::buttonTexture);
    UnloadTexture(textures::menu::sliderBarTexture);
    UnloadTexture(textures::menu::sliderHandleTexture);
    UnloadTexture(textures::menu::raylibLogoTexture);
    UnloadFont(textures::myfont);

    audio::unloadSoundsFromMemory(audio::soundsVariables);
    audio::unloadMusicFromMemory(audio::musicVariables);

    if (IsWindowFullscreen()) { ToggleFullscreen(); } // Go to windows mode before closing to avoid crashing opengl. 

    //CloseAudioDevice();
    CloseWindow();

}
void Arkanoid::input()
{

    inputRelated::inputGame();

}
void Arkanoid::update()
{

    updateGame();

}
void Arkanoid::draw()
{

   drawGame();

}
void Arkanoid::play()
{

    while (!WindowShouldClose() && sceneRelated::playing)
    {

        input();
        update();
        draw();

    }

}