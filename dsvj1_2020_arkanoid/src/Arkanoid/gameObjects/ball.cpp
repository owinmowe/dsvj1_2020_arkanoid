#include "ball.h"
#include "Arkanoid/ExternalVariables/external_variables.h"
#include "game_objects.h"

namespace gameObjects
{

	Ball::Ball()
	{
		radius = BALL_BASE_RADIUS * screenRelated::SCREEN_WIDTH;
		pos = { 0,0 };
		velocity = { 0,0 };
		speedModifier = 1.f;
		texture_Ptr = &textures::ballTexture;
		active = false;
		fireball = false;
		currentFireBallTime = 0;
	}
	Ball::~Ball()
	{

	}
	void Ball::startBall()
	{
		velocity.x += BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
		velocity.y += BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
	}
	void Ball::pseudoRandomBallDirection(int seed)
	{
		switch (seed)
		{
		case 1:
			velocity.x = BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
			velocity.y = BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
			break;
		case 2:
			velocity.x = BALL_BASE_SPEED * -speedModifier * screenRelated::SCREEN_WIDTH;
			velocity.y = BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
			break;
		case 3:
			velocity.x = BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
			velocity.y = BALL_BASE_SPEED * -speedModifier * screenRelated::SCREEN_WIDTH;
			break;
		case 4:
			velocity.x = BALL_BASE_SPEED * -speedModifier * screenRelated::SCREEN_WIDTH;
			velocity.y = BALL_BASE_SPEED * -speedModifier * screenRelated::SCREEN_WIDTH;
			break;
		default:
			velocity.x = BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
			velocity.y = BALL_BASE_SPEED * speedModifier * screenRelated::SCREEN_WIDTH;
			break;
		}
	}
	void Ball::updateBall()
	{

		previousPos = pos;
		pos.x += velocity.x * speedModifier *  GetFrameTime();
		pos.y += velocity.y * speedModifier * GetFrameTime();
		if(isFireball())
		{
			currentFireBallTime -= GetFrameTime();
			if(currentFireBallTime < 0)
			{
				fireball = false;
				texture_Ptr = &gameObjects::textures::ballTexture;
			}
		}

	}
	void Ball::moveBallX(int pos_X)
	{
		pos.x = static_cast<float>(pos_X);
	}
	void Ball::invertVelocityX()
	{
		velocity.x = -velocity.x;
	}
	void Ball::invertVelocityY()
	{
		velocity.y = -velocity.y;
	}
	Texture2D* Ball::getTexture()
	{
		return texture_Ptr;
	}
	float Ball::getRadius()
	{
		return radius;
	}
	float Ball::getSpeedModifier()
	{
		return speedModifier;
	}
	bool Ball::getPadCollision()
	{
		return padCollision;
	}
	void Ball::setPadCollision(bool padCol)
	{
		padCollision = padCol;
	}
	void Ball::startFireball()
	{
		fireball = true;
		currentFireBallTime = BASE_FIREBALL_TIME;
		texture_Ptr = &gameObjects::textures::fireball;
	}
	bool Ball::isFireball()
	{
		return fireball;
	}
	void Ball::ballToBase()
	{
		speedModifier = 1;
		slowBall = false;
		fireball = false;
		currentFireBallTime = 0;
		texture_Ptr = &gameObjects::textures::ballTexture;
	}
	void Ball::startSlowBall()
	{
		if(!slowBall)
		{
			speedModifier = SLOW_SPEED_MODIFIER;
			slowBall = true; 
		}
	}
}