#ifndef BALL_H
#define BALL_H
#include "raylib.h"

namespace gameObjects
{

	class Ball
	{
	public:
		Ball();
		~Ball();
		Vector2 pos = { 0, 0 };
		Vector2 previousPos = { 0, 0 };
		Vector2 velocity = { 0,0 };
		bool active = false;
		void startBall();
		void pseudoRandomBallDirection(int seed);
		void updateBall();
		void moveBallX(int pos_X);
		void invertVelocityX();
		void invertVelocityY();
		float getRadius();
		float getSpeedModifier();
		bool getPadCollision();
		void setPadCollision(bool padCol);
		bool isFireball();
		void startFireball();
		void startSlowBall();
		void ballToBase();
		Texture2D* getTexture();

	private:
		Texture2D* texture_Ptr;
		float speedModifier = 1.f;
		float radius = 0;
		float currentFireBallTime = 0;
		bool padCollision = false;
		bool fireball = false;
		bool slowBall = false;
	};

	const int BASE_FIREBALL_TIME = 5;
	const float SLOW_SPEED_MODIFIER = .70f;
	const float BALL_BASE_SPEED =  1 / 4.f;
	const float BALL_BASE_RADIUS = 1 / 200.f;
	const int BALL_MAX_AMMOUNT = 5;

}

#endif BALL_H
