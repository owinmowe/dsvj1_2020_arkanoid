#ifndef BOUND_H
#define BOUND_H
#include "raylib.h"
#include "Arkanoid/ExternalVariables/external_variables.h"

namespace gameObjects
{

	const int BOUND_POS_X = screenRelated::SCREEN_WIDTH * 3 / 16;
	const int BOUND_POS_Y = 0;
	const int BOUND_WIDTH = screenRelated::SCREEN_WIDTH * 5 / 8;
	const int BOUND_HEIGHT = screenRelated::SCREEN_HEIGHT;
	const int BOUND_SIZE_X = static_cast<int>(screenRelated::SCREEN_WIDTH / 54);
	const int BOUND_SIZE_Y = static_cast<int>(screenRelated::SCREEN_HEIGHT / 16.5f);

	class Bound
	{
	public:
		Bound();
		~Bound();
		Rectangle rec = { BOUND_POS_X, BOUND_POS_Y, BOUND_WIDTH, BOUND_HEIGHT };
		Vector2 boundSize = { BOUND_SIZE_X , BOUND_SIZE_Y };
		void onCollision();

	private:

	};

}
#endif BOUND_H
