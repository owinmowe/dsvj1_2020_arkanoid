#include "bound.h"
#include "game_objects.h"

namespace gameObjects
{

	Bound::Bound()
	{
		rec = { BOUND_POS_X, BOUND_POS_Y, BOUND_WIDTH, BOUND_HEIGHT };
		boundSize = { BOUND_SIZE_X , BOUND_SIZE_Y };

	}
	Bound::~Bound()
	{

	}
	void Bound::onCollision()
	{
		gameObjects::events::boundHit = true;
	}

}