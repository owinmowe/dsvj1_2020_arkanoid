#ifndef POWER_UP_H
#define POWER_UP_H

#include "raylib.h"

namespace gameObjects
{

	const float POWER_UP_BASE_RADIUS = 1 / 150.f;
	const float POWER_UP_BASE_SPEED = 3 / 12.f;
	const int POWER_UPS_AMMOUNT = 15;
	const int POWER_UPS_TYPES_COUNT = 4;

	class PowerUp
	{
	public:
		PowerUp();
		~PowerUp();
		bool active = false;
		void updateFall();
		void setPosition(Vector2 newPos);
		Vector2 getPosition();
		virtual void onCollision();
		Texture2D* getTexture();
		Color getColor();
		float getRadius();
		void setColor(Color newColor);

	protected:
		Texture2D* texture = nullptr;
		Vector2 pos = { 0, 0 };
		float radius = 0;
		Color color = WHITE;
	};

	class MultiballPowerUp : public PowerUp
	{
	public:
		MultiballPowerUp();
		void onCollision() override;
	};

	class FireBallPowerUp : public PowerUp
	{
	public:
		FireBallPowerUp();
		void onCollision() override;
	};

	class PaddlePowerUp : public PowerUp
	{
	public:
		PaddlePowerUp();
		void onCollision() override;
	};

	class BallSpeedPowerUp : public PowerUp
	{
	public:
		BallSpeedPowerUp();
		void onCollision() override;
	};

	PowerUp* getRandomPowerUps();

}

#endif POWER_UP_H
