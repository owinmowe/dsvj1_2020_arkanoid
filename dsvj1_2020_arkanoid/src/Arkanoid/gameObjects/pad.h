#ifndef PAD_H
#define PAD_H

#include "raylib.h"

namespace gameObjects
{

	const float PAD_BASE_WIDTH = 1 / 12.f;
	const float PAD_BASE_HEIGHT = 1 / 25.f;
	const float PAD_BASE_SPEED = 5 / 12.f;

	class Pad
	{
	public:
		Pad();
		~Pad();
		Rectangle rec = { 0, 0, 0, 0 };
		void randomRezise();
		void padToBaseSize();
		void move(int speed);
		void onCollision();

	private:
	};

}

#endif PAD_H

