#include "pad.h"
#include "game_objects.h"
#include "Arkanoid/ExternalVariables/external_variables.h"

namespace gameObjects
{

	Pad::Pad()
	{
		rec = { 0, 0, PAD_BASE_WIDTH * screenRelated::SCREEN_WIDTH, PAD_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT };
	}
	Pad::~Pad()
	{

	}
	void Pad::move(int speed)
	{
		rec.x += speed;
	}
	void Pad::randomRezise()
	{
		int aux = GetRandomValue(0, 2);
		if(aux == 1)
		{
			rec.width *= 1.25f;
			gameObjects::textures::padTexture.width = static_cast<int>(gameObjects::textures::padTexture.width * 1.25f);
		}
		else
		{
			rec.width *= 0.75f;
			gameObjects::textures::padTexture.width = static_cast<int>(gameObjects::textures::padTexture.width * 0.75f);
		}
	}
	void Pad::padToBaseSize()
	{
		rec.width = PAD_BASE_WIDTH * screenRelated::SCREEN_WIDTH;
		gameObjects::textures::padTexture.width = static_cast<int>(PAD_BASE_WIDTH * screenRelated::SCREEN_WIDTH);
	}
	void Pad::onCollision()
	{
		events::padHit = true;
	}

}