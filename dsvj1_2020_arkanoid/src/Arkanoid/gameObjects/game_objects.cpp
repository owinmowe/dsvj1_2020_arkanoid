#include "game_objects.h"

namespace gameObjects
{

	namespace textures
	{

		Texture2D ballTexture;
		Texture2D padTexture;
		Texture2D outerBoundsTexture;

		Texture2D commonBrickTexture;
		Texture2D indestructibleBrickTexture;
		Texture2D explosiveBrickTexture;

		Texture2D multiballPowerUp;
		Texture2D fireballPowerUp;
		Texture2D fireball;
		Texture2D paddlePowerUp;
		Texture2D ballSpeedPowerUp;

	}


	namespace events
	{

		bool boundHit = false;
		bool padHit = false;
		bool brickCommonHit = false;
		bool brickIndestructibleHit = false;
		bool brickExplosiveHit = false;

		bool powerUpPickedUp = false;
		bool multiballPowerUp = false;
		bool fireballPowerUp = false;
		bool paddlePowerUp = false;
		bool ballSpeedPowerUp = false;

		void resetGameObjectsEvents()
		{
			boundHit = false;
			padHit = false;
			brickCommonHit = false;
			brickIndestructibleHit = false;
			brickExplosiveHit = false;
			powerUpPickedUp = false;
			multiballPowerUp = false;
			fireballPowerUp = false;
			paddlePowerUp = false;
			ballSpeedPowerUp = false;
		}

	}

}