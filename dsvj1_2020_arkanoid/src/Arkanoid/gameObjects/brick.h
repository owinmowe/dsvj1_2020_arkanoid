#ifndef BRICK_H
#define BRICK_H

#include "raylib.h"
#include "power_up.h"

namespace gameObjects
{

	const float BRICKS_STARTING_POSITION = 15 / 64.f;
	const int BRICKS_ROW = 8;
	const int BRICKS_COL = 15;
	const int POWER_UP_CHANCE = 5;
	const float BRICKS_BASE_WIDTH = 1/32.f;
	const float BRICKS_BASE_HEIGHT = 1/18.f;
	const float BRICKS_SEPARATION = 0;

	class Brick
	{
	public:
		Brick();
		~Brick();
		int id;
		bool active = false;
		Rectangle rec = { 0, 0, BRICKS_BASE_WIDTH, BRICKS_BASE_HEIGHT };
		Texture2D* getTexturePtr();
		virtual void onCollision(bool destroy);
		bool powerUpCreationLogic();
		Color getColor();
		int getCurrentLifes();
		void setDestroyBool(bool destroyState);

	protected:
		bool collided = false;
		bool destroyed = false;
		Texture2D* texture = nullptr;
		Color currentColor = WHITE;
		int score = 0;
		int currentLifes = 0;
	};

	class CommonBrick : public Brick
	{
	public:
		CommonBrick();
		CommonBrick(int life);
		~CommonBrick();
		void onCollision(bool destroy) override;
		void setLifes(int newLife);

	private:
		void setColorByLife();
	};

	class IndestructibleBrick : public Brick
	{
	public:
		IndestructibleBrick();
		~IndestructibleBrick();
		void onCollision(bool destroy) override;

	private:
	};

	class ExplosiveBrick : public Brick
	{
	public:
		ExplosiveBrick();
		~ExplosiveBrick();
		void onCollision(bool destroy) override;

	private:
	};

}

#endif BRICK_H