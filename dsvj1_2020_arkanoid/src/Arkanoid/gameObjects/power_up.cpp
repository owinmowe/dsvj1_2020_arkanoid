#include "power_up.h"
#include "Arkanoid/ExternalVariables/external_variables.h"
#include "Arkanoid/gameObjects/game_objects.h"

namespace gameObjects
{

	PowerUp::PowerUp()
	{
		pos = { 0, 0 };
		radius = POWER_UP_BASE_RADIUS * screenRelated::SCREEN_WIDTH;
		active = false;
		texture = nullptr;
		color = WHITE;
	}
	PowerUp::~PowerUp()
	{

	}
	void PowerUp::updateFall()
	{
		pos.y += POWER_UP_BASE_SPEED * GetFrameTime() * screenRelated::SCREEN_HEIGHT;
		if(pos.y > screenRelated::SCREEN_HEIGHT)
		{
			active = false;
		}
	}
	void PowerUp::onCollision()
	{
		return;
	}
	Vector2 PowerUp::getPosition()
	{
		Vector2 aux = { pos.x, pos.y };
		return aux;
	}
	void PowerUp::setPosition(Vector2 newPos)
	{
		pos.x = newPos.x;
		pos.y = newPos.y;
	}
	Texture2D* PowerUp::getTexture()
	{
		return texture;
	}
	Color PowerUp::getColor()
	{
		return color;
	}
	void PowerUp::setColor(Color newColor)
	{
		color = newColor;
	}
	float PowerUp::getRadius()
	{
		return radius;
	}

	MultiballPowerUp::MultiballPowerUp()
	{
		PowerUp::PowerUp();
		setColor(BLUE);
		texture = &gameObjects::textures::multiballPowerUp;
	}
	void MultiballPowerUp::onCollision()
	{
		active = false;
		events::powerUpPickedUp = true;
		events::multiballPowerUp = true;
	}
	FireBallPowerUp::FireBallPowerUp()
	{
		PowerUp::PowerUp();
		setColor(RED);
		texture = &gameObjects::textures::fireballPowerUp;
	}
	void FireBallPowerUp::onCollision()
	{
		active = false;
		events::powerUpPickedUp = true;
		events::fireballPowerUp = true;
	}
	PaddlePowerUp::PaddlePowerUp()
	{
		PowerUp::PowerUp();
		setColor(YELLOW);
		texture = &gameObjects::textures::paddlePowerUp;
	}
	void PaddlePowerUp::onCollision()
	{
		active = false;
		events::powerUpPickedUp = true;
		events::paddlePowerUp = true;
	}
	BallSpeedPowerUp::BallSpeedPowerUp()
	{
		PowerUp::PowerUp();
		setColor(GREEN);
		texture = &gameObjects::textures::ballSpeedPowerUp;
	}
	void BallSpeedPowerUp::onCollision()
	{
		active = false;
		events::powerUpPickedUp = true;
		events::ballSpeedPowerUp = true;
	}

	PowerUp* getRandomPowerUps()
	{
		int aux = GetRandomValue(1, gameObjects::POWER_UPS_TYPES_COUNT);

		switch (aux)
		{
		case 1:
			return new MultiballPowerUp;
			break;
		case 2:
			return new FireBallPowerUp;
			break;
		case 3:
			return new PaddlePowerUp;
			break;
		case 4:
			return new BallSpeedPowerUp;
			break;
		default:
			return new MultiballPowerUp;
			break;
		}
	}
}