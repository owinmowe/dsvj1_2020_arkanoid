#include "brick.h"
#include "game_objects.h"
#include "Arkanoid/ExternalVariables/external_variables.h"

namespace gameObjects
{

	Brick::Brick()
	{
		rec = { 0 , 0, BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH, BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT};
		id = 0;
		score = 0;
		texture = nullptr;
		active = false;
		destroyed = false;
	}
	Brick::~Brick()
	{
		
	}
	void Brick::onCollision(bool destroy)
	{
		return;
	}
	Texture2D* Brick::getTexturePtr()
	{
		return texture;
	}
	Color Brick::getColor()
	{
		return currentColor;
	}
	int Brick::getCurrentLifes()
	{
		return currentLifes;
	}
	bool Brick::powerUpCreationLogic()
	{
		if(destroyed)
		{
			int aux = GetRandomValue(1, POWER_UP_CHANCE);
			if (aux == 1)
			{
				return true;
			}
			else
			{
				destroyed = false;
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	void Brick::setDestroyBool(bool destroyState)
	{
		destroyed = destroyState;
	}

	CommonBrick::CommonBrick()
	{
		rec = { 0 , 0, BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH, BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT };
		texture = &gameObjects::textures::commonBrickTexture;
		currentLifes = 1;
		id = currentLifes + 2;

	}
	CommonBrick::~CommonBrick()
	{

	}
	CommonBrick::CommonBrick(int life)
	{
		texture = &gameObjects::textures::commonBrickTexture;
		currentLifes = life;
	}
	void CommonBrick::setLifes(int newLife)
	{
		currentLifes = newLife;
		setColorByLife();

	}
	void CommonBrick::setColorByLife() 
	{
		switch (currentLifes)
		{
		case 1:
			score = 15;
			currentColor = GREEN;
			break;
		case 2:
			score = 30;
			currentColor = RED;
			break;
		case 3:
			score = 60;
			currentColor = BLUE;
			break;
		case 4:
			score = 120;
			currentColor = PINK;
			break;
		case 5:
			score = 240;
			currentColor = YELLOW;
			break;
		case 6:
			score = 480;
			currentColor = ORANGE;
			break;
		case 7:
			score = 960;
			currentColor = MAGENTA;
			break;
		default:
			currentColor = WHITE;
			break;
		}
	}
	void CommonBrick::onCollision(bool fireball)
	{
		events::brickCommonHit = true;
		currentLifes--;
		if(currentLifes <= 0 || fireball)
		{
			currentLifes = 0;
			active = false;
			destroyed = true;
			in_game::gameScoreRelated::currentScore += score;
			in_game::gameScoreRelated::remainingBlocks--;
		}
	}

	IndestructibleBrick::IndestructibleBrick()
	{
		rec = { 0 , 0, BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH, BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT };
		texture = &gameObjects::textures::indestructibleBrickTexture;
		currentLifes = 1;
		id = 1;
	}
	IndestructibleBrick::~IndestructibleBrick()
	{

	}
	void IndestructibleBrick::onCollision(bool fireball)
	{
		events::brickIndestructibleHit = true;
		if (fireball)
		{
			currentLifes = 0;
			destroyed = true;
			active = false;
		}
	}

	ExplosiveBrick::ExplosiveBrick()
	{
		rec = { 0 , 0, BRICKS_BASE_WIDTH * screenRelated::SCREEN_WIDTH, BRICKS_BASE_HEIGHT * screenRelated::SCREEN_HEIGHT };
		texture = &gameObjects::textures::explosiveBrickTexture;
		currentLifes = 1;
		id = 2;
	}
	ExplosiveBrick::~ExplosiveBrick()
	{

	}
	void ExplosiveBrick::onCollision(bool fireball)
	{
		events::brickExplosiveHit = true;
		currentLifes--;
		if (currentLifes <= 0 || fireball)
		{
			currentLifes = 0;
			active = false;
			destroyed = true;
			in_game::gameScoreRelated::currentScore += score;
			in_game::gameScoreRelated::remainingBlocks--;
		}
	}

}