#ifndef GAME_OBJECTS_H
#define GAME_OBJECTS_H

#include "raylib.h"
#include "ball.h"
#include "brick.h"
#include "pad.h"
#include "bound.h"
#include "power_up.h"

namespace gameObjects
{

	namespace textures
	{

		extern Texture2D ballTexture;
		extern Texture2D padTexture;
		extern Texture2D fireball;
		extern Texture2D outerBoundsTexture;
		const char BALL_PATH_FILE[] = "res/assets/texture/gameObjects/ball.png";
		const char FIREBALL_PATH_FILE[] = "res/assets/texture/gameObjects/fireball.png";
		const char PAD_PATH_FILE[] = "res/assets/texture/gameObjects/pad.png";
		const char OUTER_BOUNDS_PATH_FILE[] = "res/assets/texture/gameObjects/bounds.png";

		extern Texture2D commonBrickTexture;
		extern Texture2D indestructibleBrickTexture;
		extern Texture2D explosiveBrickTexture;
		const char COMMON_BRICK_PATH_FILE[] = "res/assets/texture/gameObjects/common_brick.png";
		const char INDESTRUCTIBLE_BRICK_PATH_FILE[] = "res/assets/texture/gameObjects/indestructible_brick.png";
		const char EXPLOSIVE_BRICK_PATH_FILE[] = "res/assets/texture/gameObjects/explosive_brick.png";

		extern Texture2D multiballPowerUp;
		extern Texture2D fireballPowerUp;
		extern Texture2D paddlePowerUp;
		extern Texture2D ballSpeedPowerUp;
		const char MULTIBALL_POWER_UP_PATH_FILE[] = "res/assets/texture/gameObjects/powerUps/multiball.png";
		const char FIREBALL_POWER_UP_PATH_FILE[] = "res/assets/texture/gameObjects/powerUps/fireball.png";
		const char PADDLE_POWER_UP_PATH_FILE[] = "res/assets/texture/gameObjects/powerUps/paddle.png";
		const char BALL_SPEED_POWER_UP_PATH_FILE[] = "res/assets/texture/gameObjects/powerUps/ball_speed.png";

	}

	namespace events
	{

		extern bool boundHit;
		extern bool padHit;
		extern bool brickCommonHit;
		extern bool brickIndestructibleHit;
		extern bool brickExplosiveHit;

		extern bool powerUpPickedUp;
		extern bool multiballPowerUp;
		extern bool fireballPowerUp;
		extern bool paddlePowerUp;
		extern bool ballSpeedPowerUp;

		void resetGameObjectsEvents();

	}


}

#endif