#include "audio.h"
#include "Arkanoid/ExternalVariables/external_variables.h"
#include "Arkanoid/gameObjects/game_objects.h"

namespace audio
{

    GameMusic musicVariables;
    float musicVolume = 0.1f;

    GameSounds soundsVariables;
    float soundVolume = 0.1f;

    void setAllSoundVolume()
    {

        SetSoundVolume(soundsVariables.menuMoveSound, soundVolume);
        SetSoundVolume(soundsVariables.menuSelectSound, soundVolume);
        SetSoundVolume(soundsVariables.boundHit, soundVolume);
        SetSoundVolume(soundsVariables.padHit, soundVolume);
        SetSoundVolume(soundsVariables.brickCommonHit, soundVolume);
        SetSoundVolume(soundsVariables.brickIndestructibleHit, soundVolume);
        SetSoundVolume(soundsVariables.brickExplosiveHit, soundVolume);

    }

    void setAllMusicVolume()
    {

        SetMusicVolume(musicVariables.GameMusic, musicVolume);
        SetMusicVolume(musicVariables.MenuMusic, musicVolume);

    }

    void menuAudio(bool moveEvent, bool selectEvent)
    {

        if (moveEvent) { PlaySoundMulti(soundsVariables.menuMoveSound); }
        if (selectEvent) { PlaySoundMulti(soundsVariables.menuSelectSound); }

        UpdateMusicStream(musicVariables.MenuMusic);

    }

    void gameAudio()
    {

        UpdateMusicStream(musicVariables.GameMusic);
        if (gameObjects::events::boundHit)               { PlaySoundMulti(soundsVariables.boundHit); }
        if (gameObjects::events::padHit)                 { PlaySoundMulti(soundsVariables.padHit); }
        if (gameObjects::events::brickCommonHit)         { PlaySoundMulti(soundsVariables.brickCommonHit); }
        if (gameObjects::events::brickIndestructibleHit) { PlaySoundMulti(soundsVariables.brickIndestructibleHit); }
        if (gameObjects::events::brickExplosiveHit)      { PlaySoundMulti(soundsVariables.brickExplosiveHit); }

    }

    void loadSoundsInMemory(GameSounds& gameSounds)
    {

        gameSounds.menuMoveSound = LoadSound(MENU_MOVE_SOUND_FILE_PATH);
        gameSounds.menuSelectSound = LoadSound(MENU_SELECT_SOUND_FILE_PATH);
        gameSounds.boundHit = LoadSound(BOUND_HIT_SOUND_FILE_PATH);
        gameSounds.padHit = LoadSound(PAD_HIT_SOUND_FILE_PATH);
        gameSounds.brickCommonHit = LoadSound(BRICK_COMMON_HIT_SOUND_FILE_PATH);
        gameSounds.brickIndestructibleHit = LoadSound(BRICK_INDESTRUCTIBLE_HIT_SOUND_FILE_PATH);
        gameSounds.brickExplosiveHit = LoadSound(BRICK_EXPLOSIVE_HIT_SOUND_FILE_PATH);
        setAllSoundVolume();

    }

    void unloadSoundsFromMemory(GameSounds& gameSounds)
    {

        UnloadSound(gameSounds.menuMoveSound);
        UnloadSound(gameSounds.menuSelectSound);
        UnloadSound(gameSounds.boundHit);
        UnloadSound(gameSounds.padHit);
        UnloadSound(gameSounds.brickCommonHit);
        UnloadSound(gameSounds.brickIndestructibleHit);
        UnloadSound(gameSounds.brickExplosiveHit);

    }

    void loadMusicInMemory(GameMusic& gameMusic)
    {

        gameMusic.GameMusic = LoadMusicStream(MATCH_MUSIC_FILE_PATH);
        gameMusic.MenuMusic = LoadMusicStream(MENU_MUSIC_FILE_PATH);
        setAllMusicVolume();

    }

    void unloadMusicFromMemory(GameMusic& gameMusic)
    {

        UnloadMusicStream(gameMusic.GameMusic);
        UnloadMusicStream(gameMusic.MenuMusic);

    }

}