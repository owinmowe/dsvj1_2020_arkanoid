#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace audio
{

	struct GameSounds
	{

		Sound menuMoveSound;
		Sound menuSelectSound;
		Sound boundHit;
		Sound padHit;
		Sound brickCommonHit;
		Sound brickIndestructibleHit;
		Sound brickExplosiveHit;

	};

	struct GameMusic
	{

		Music GameMusic;
		Music MenuMusic;

	};

	extern GameMusic musicVariables;
	extern float musicVolume;

	extern GameSounds soundsVariables;
	extern float soundVolume;

	void menuAudio(bool moveEvent, bool selectEvent);

	void gameAudio();

	void loadSoundsInMemory(GameSounds& gameSounds);

	void setAllSoundVolume();

	void unloadSoundsFromMemory(GameSounds& gameSounds);

	void loadMusicInMemory(GameMusic& gameMusic);

	void unloadMusicFromMemory(GameMusic& gameMusic);

	void setAllMusicVolume();


	const char MENU_MOVE_SOUND_FILE_PATH[] = "res/assets/audio/sounds/menuMoveSound.wav";
	const char MENU_SELECT_SOUND_FILE_PATH[] = "res/assets/audio/sounds/menuSelectSound.wav";

	const char BOUND_HIT_SOUND_FILE_PATH[] = "res/assets/audio/sounds/boundHit.wav";
	const char PAD_HIT_SOUND_FILE_PATH[] = "res/assets/audio/sounds/padHit.wav";
	const char BRICK_COMMON_HIT_SOUND_FILE_PATH[] = "res/assets/audio/sounds/brickCommonHit.wav";
	const char BRICK_INDESTRUCTIBLE_HIT_SOUND_FILE_PATH[] = "res/assets/audio/sounds/brickIndestructibleHit.wav";
	const char BRICK_EXPLOSIVE_HIT_SOUND_FILE_PATH[] = "res/assets/audio/sounds/brickExplosiveHit.wav";

	const char MATCH_MUSIC_FILE_PATH[] = "res/assets/audio/music/matchMusic.mp3";
	const char MENU_MUSIC_FILE_PATH[] = "res/assets/audio/music/menuMusic.mp3";

}

#endif AUDIO_H
